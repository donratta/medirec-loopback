module.exports = function(Appointment) {


    Appointment.getAppointmentWithInfo = function(cb) {
        Appointment.find({include:['patient','doctor']},function(err,result){
            if(err){
                cb(err,null);
                return;
            }
            cb(null,result);
        })
    }
    Appointment.remoteMethod(
        'getAppointmentWithInfo',
        {
            returns: {arg: 'data', type: 'object'},
            http: {path:'/getAppointmentWithInfo', verb: 'get'}
        }
    );

    Appointment.getDoctorAppointmentsUpcoming = function(id,cb) {
        Appointment.find({
                where: {
                    doctorId:id,
                    dateTime:{gt:new Date()}
                },
                include:['patient','doctor']
            }).then(function(data){
                cb(null,data);
            });
    }
    Appointment.remoteMethod(
        'getDoctorAppointmentsUpcoming',
        {
            accepts: {arg: 'id', type: 'string'},
            returns: {arg: 'data', type: 'object'},
            http: {path:'/getDoctorAppointmentsUpcoming', verb: 'get'}
        }
    );


    Appointment.getAllUpcomingAppointments = function(cb) {
        Appointment.find({
            where: {
                dateTime:{gt:new Date()}
            },
            include:['patient','doctor']
        }).then(function(data){
                cb(null,data);
            });
    }
    Appointment.remoteMethod(
        'getAllUpcomingAppointments',
        {
            returns: {arg: 'data', type: 'object'},
            http: {path:'/getAllUpcomingAppointments', verb: 'get'}
        }
    );








};
