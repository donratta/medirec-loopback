module.exports = function(Hmo) {

    Hmo.validatesUniquenessOf('code');
    Hmo.validatesUniquenessOf('name');

};
