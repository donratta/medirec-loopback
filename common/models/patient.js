
var uuid = require('uuid');
module.exports = function(Patient) {
    var app = require('../../server/server');

    Patient.beforeValidate = function(next, modelInstance) {
        //your logic goes here - don't use modelInstance
        //generate the hash here for the offial ID here
        var hash = uuid.v4();
        hash = hash.split('-');
        hash = hash[hash.length-1];
        //TODO - Append the hospital Medirec  HERE for cloud access
        if(!this.officialId){
            this.officialId = process.env.HOSPITALNUMBER+"-"+hash ;
        }
        this.firstName = this.firstName.toLowerCase();
        this.lastName = this.lastName.toLowerCase();
        next();
    };
    Patient.validatesUniquenessOf('officialId');


    Patient.remoteMethod(
        'searchRecord',
        {
            accepts: {arg: 'searchParam', type: 'string'},
            returns: {arg: 'data', type: 'object'},
            http: {path:'/searchRecord', verb: 'get'}
        }
    );

    Patient.searchRecord = function(searchParam,cb) {
        Patient.find({where:{
            or:[{firstName:{like:new RegExp(searchParam.toLowerCase(), "i")}},
                {lastName:{like:new RegExp(searchParam.toLowerCase(), "i")}},
                {officialId:{like:new RegExp(searchParam.toLowerCase(), "i")}}]
            }},
            function(err,result){
            if(err){
                cb(err,null);
                return;
            }
            cb(null,result);
        })
    }

};
