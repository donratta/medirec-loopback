var app = require('../../server/server');
var uuid = require('uuid');
module.exports = function(Admin) {
    Admin.beforeValidate = function(next, modelInstance) {
        //your logic goes here - don't use modelInstance
        //generate the hash here for the offial ID here
        var hash = uuid.v4();
        hash = hash.split('-');
        hash = hash[hash.length-1];
        //TODO - Append the hospital Medirec  HERE for cloud access
        this.officialId = hash ;
        this.firstName = this.firstName.toLowerCase();
        this.lastName = this.lastName.toLowerCase();
        next();
    };

    Admin.afterCreate = function(next) {
        var nexter = next;
        //Add the role doctor to the newly created doctor
        var newId = this.id
        app.models.Role.findOne({name:'admin'})
            .then(function(role){
                role.principals.create({
                    principalType: app.models.RoleMapping.USER,
                    principalId: newId
                }, function(err, principal) {
                    nexter();
                });
            })
    };


};
