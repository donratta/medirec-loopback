module.exports = function(Hospital) {

    Hospital.getHospitalSetting = function(cb) {

        Hospital.find({}).
            then(function(data){
                cb(null,data[0]);
            })

    }
    Hospital.remoteMethod(
        'getHospitalSetting',
        {
            returns: {arg: 'data', type: 'object'},
            http: {path:'/getHospitalSetting', verb: 'get'}
        }
    );

    Hospital.validatesUniquenessOf('hospitalNumber');

};
