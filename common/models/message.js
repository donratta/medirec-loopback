var app = require('../../server/server');
module.exports = function(Message) {
    Message.sendMessage = function(req,cb) {
        // console.log(req.body);
        //create the message head and
        var newMessageObject = {}
        newMessageObject.authorId = req.body.sender;
        newMessageObject.body = req.body.body;
        newMessageObject.subject = req.body.subject;
        Message.create(newMessageObject,function(err,newMess){
            if(err){
                cb(err,null);
                return;
            }
            //add the recipients here

            var usersToGo = [];
            for(var i=0;i<req.body.recipients.length;i++){
                var obj = {};
                obj.isRead = false;
                obj.isStarred = false;
                obj.doctorId = req.body.recipients[i].id;
                obj.createdAt = new Date();
                obj.messageId = newMess.id;
                usersToGo.push(obj)
            }
            app.models.MessageUser.create(usersToGo,function(err,result){
                if(err){
                    cb(err,null);
                    return;
                }
                cb(null,newMess);
            })

        })
    }
    Message.remoteMethod(
        'sendMessage',
        {
            accepts: {arg: 'req', type: 'object', 'http': {source: 'req'}},
            returns: {arg: 'data', type: 'object'},
            http: {path:'/sendMessage', verb: 'post'}

        }
    );



    /*this is remote method to get my messages*/
    Message.getMyMessages = function(id,cb) {
        app.models.MessageUser.find({ where: {doctorId:id} ,include:{message:['doctor']}},function(err,result){
            if(err){
                cb(err,null);
                return;
            }
            cb(null,result);
        })
    }
    Message.remoteMethod(
        'getMyMessages',
        {
            accepts: {arg: 'id', type: 'string'},
            returns: {arg: 'data', type: 'object'},
            http: {path:'/getMyMessages', verb: 'get'}

        }
    )




};
