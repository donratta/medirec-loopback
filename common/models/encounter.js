module.exports = function(Encounter) {

    Encounter.getFullEncounter = function(id, cb) {
        var app = Encounter.app;
        var Patient = app.models.Patient;
        var Doctor = app.models.Doctor;
        Encounter.findById(id,{include:['patient','doctor']},function(err,data){
            if(err){
                cb(err,null);
                return;
            }
            cb(null,data);

        })
    }
    Encounter.remoteMethod(
        'getFullEncounter',
        {
            accepts: {arg: 'id', type: 'string'},
            returns: {arg: 'data', type: 'object'},
            http: {path:'/getFullEncounter', verb: 'get'}
        }
    );

    Encounter.getEncountersNotClosed = function(cb) {
        Encounter.find({where:{or:[{status:'Wait'},{status:'withDoctor'}]},include:['patient','doctor']},function(err,result){
                if(err){
                    cb(err,null);
                    return;
                }
                cb(null,result);

        })

    }
    Encounter.remoteMethod(
           'getEncountersNotClosed',
    {
        returns: {arg: 'data', type: 'object'},
        http: {path:'/getEncountersNotClosed', verb: 'get'}

    }
    );
    Encounter.getEncountersForDoctorWaiting = function(id,cb) {
        Encounter.find({where:{doctorId:id},include:['patient']},function(err,result){
                if(err){
                    cb(err,null);
                    return;
                }
                cb(null,result);
        })
    }
    Encounter.remoteMethod(
           'getEncountersForDoctorWaiting',
    {
        returns: {arg: 'data', type: 'object'},
        accepts: {arg: 'id', type: 'string'},
        http: {path:'/getEncountersForDoctorWaiting', verb: 'get'}
    }
    );


    Encounter.closeEncounter = function(req,cb) {
        var app = Encounter.app;
        var encounter = req.body;
        Encounter.findById(encounter.id,function(err,result){
                if(err){
                    cb(err,null);
                    return;
                }
                result.status = 'closed';
                result.save(function(err,data){
                    if(err){
                        cb(err,null);
                        return;
                    }

                    //create a new billing record
                    var newBillingObject = {};
                    newBillingObject.patientId = encounter.patient.id;
                    newBillingObject.encounterId = encounter.id;
                    newBillingObject.totalCost = 0;
                    newBillingObject.consultationCost = 0;
                    newBillingObject.labCost = 0;
                    newBillingObject.pharmacyCost = 0;
                    newBillingObject.billingMode = 'unset';
                    newBillingObject.otherCost = 0;
                    newBillingObject.createdAt = new Date();
                    app.models.EncounterBilling.create(newBillingObject,function(err,billingObject){
                        if(err){
                            cb(err,null);
                        }


                        cb(null,billingObject);
                    })
                })

        })
    }
    Encounter.remoteMethod(
           'closeEncounter',
    {
        accepts: {arg: 'req', type: 'object', 'http': {source: 'req'}},
        returns: {arg: 'data', type: 'object'},
        http: {path:'/closeEncounter', verb: 'post'}
    }
    );



    Encounter.Test = function(res,cb) {

        res.data = {ratta:"ratta"}

        cb(null,res);

    }
    Encounter.remoteMethod(
           'Test',
    {
        accepts: {arg: 'res', type: 'object', 'http': {source: 'res'}},
        http: {path:'/Test', verb: 'get'}
    }
    );
};
