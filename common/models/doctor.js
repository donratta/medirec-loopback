var uuid = require('uuid');
var app = require('../../server/server');
module.exports = function(Doctor) {
    Doctor.search = function(search_string,callback){
          var app = Doctor.app;
            Doctor.find({where:{
              or:[{firstName:{like:'%'+search_string+'%'}},{lastName:{like:"%"+search_string+"%"}}]
          }},function(err,results){
              if(err){
                  callback(err,results);
                  return;
              }
              callback(null,results);
          })
    }

    Doctor.remoteMethod('search',  {
        accepts: [{arg: 'search_string', type: 'String'}],
        returns: { arg: 'data', type: 'Object', http: { source: 'body' } },
        http: {path:'/search', verb: 'get'},
        description:'search for a doctor record with either the first or last name'
    })

    Doctor.beforeValidate = function(next, modelInstance) {
        //your logic goes here - don't use modelInstance
        //generate the hash here for the offial ID here
        var hash = uuid.v4();
        hash = hash.split('-');
        hash = hash[hash.length-1];
        //TODO - Append the hospital Medirec  HERE for cloud access
        this.officialId = hash ;
        this.firstName = this.firstName.toLowerCase();
        this.lastName = this.lastName.toLowerCase();
        next();
    };

   Doctor.afterCreate = function(next) {
       var nexter = next;
        //Add the role doctor to the newly created doctor
            var newId = this.id
            app.models.Role.findOne({name:'doctor'})
                .then(function(role){
                    role.principals.create({
                        principalType: app.models.RoleMapping.USER,
                        principalId: newId
                    }, function(err, principal) {
                        nexter();
                    });
                })
    };
};
