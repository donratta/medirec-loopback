angular.module('Medirec.controller.sideMenu', ['ngRoute']).
    controller('SideMenuController',['$scope','$location','LoginManager',function(scope,location,LoginManager){
        scope.currentUser = {}
        scope.goToPatient = function(){
            location.path('/patients');
        }
        scope.goToOfficial = function(){
            location.path('/officials')
        }
        scope.goToHome = function(){
            location.path('/home')
        }
        scope.goToEncounters = function(){
            location.path('/encounters')
        }
        scope.goToHospitalSetting = function(){
            location.path('/hospitalSettings')
        },
        scope.goToMessages= function(){
            location.path('/messages')
        }
        scope.goToAppointments = function(){
            location.path('/appointments')
        }
        scope.goToPharmacy = function(){
            location.path('/pharmacy');
        }
        scope.goToDataManagement=function(){
            location.path('/dataManagement')

        }

    }])