angular.module('Medirec.controller.banner', ['ngRoute']).
    controller('BannerController',['$scope','$location','LoginManager','UserManager','HospitalManager','MessageManager',function(scope,location,LoginManager,UserManager,HospitalManager,MessageManager){
        scope.currentUser = {}
        scope.hospital = {};
        scope.inboxItems = [];
        scope.unreadCount = 0;
        scope.goToPatient = function(){
            location.path('/patient');
        }

        //load the user scop
        UserManager.loadUserProfile(function(err,data){
            if(err){
                toastr.error("Cannot load the user data");
                return;
            }
            scope.currentUser = data;

        }) ;
            (scope.getHospitalSetting = function(){
                HospitalManager.getHospitalSettings()
                    .then(function(data){
                        scope.hospital = data.data.data;
                        console.log(data)

                    },function(data,status){
                        toastr.error("Cannot get hospital setting:"+data.statusText);
                    })
            })()  ;
        (scope.getAllInbox = function(){
            MessageManager.getUserInboxMessages(localStorage.getItem('userId'))
                .then(function(data,status){
                    scope.inboxItems = data.data.data;
                    console.log(scope.inboxItems);
                    var unread = _.where(scope.inboxItems,{isRead:false});
                    if(!unread){
                        scope.unreadCount = 0
                        return;
                    }
                    console.log(unread);
                    scope.unreadCount = unread.length;

                },function(data,status){
                    toastr.error("Cannot get inbox items:"+status);
                    console.log(data);
                })
        })();


        scope.logout=function(){
            localStorage.removeItem("Authorization");
            localStorage.removeItem("userId");
            location.path('/login');
        }

    }])