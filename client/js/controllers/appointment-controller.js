angular.module('Medirec.controller.appointment', ['ngRoute']).
    controller('AppointmentController',['$scope','$location','LoginManager','PatientManager','AppointmentManager','$modal','DoctorManager',function(scope,location,LoginManager,PatientManager,AppointmentManager,modal,DoctorManager){
        scope.calendarView = 'month';
        var theDate = new Date();
               scope.events = [];
        scope.calendarDay = new Date(theDate.getFullYear(),theDate.getMonth(),theDate.getDate(),0);
        scope.calendarTitle = "Title Here"

        scope.changeCalendarView = function(newView){
            scope.calendarView = newView

        };

        //load the list of appointments starting from today

            (scope.init = function(){
                scope.events = [];
           AppointmentManager.getAllAppointmentsWithInfo()
               .then(function(data,status){
                   console.log(data) ;
                   var result = data.data.data;
                   result.forEach(function(appointment){
                       var patientName = appointment.patient.firstName + ' ' + appointment.patient.lastName;
                       var doctorName = appointment.doctor.firstName + ' '+ appointment.doctor.lastName;

                       var obj = {
                           bulkObject:appointment,
                           title:patientName+ ' to see ' + doctorName, // The title of the event
                           type: 'info', // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
                           startsAt: appointment.dateTime, // A javascript date object for when the event starts
                           //endsAt: new Date(2014,8,26,15), // Optional - a javascript date object for when the event ends
                           editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable.
                           deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
                           draggable: true, //Allow an event to be dragged and dropped
                           resizable: true, //Allow an event to be resizable
                           incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
                           cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
                       }
                       scope.events.push(obj);

                   })



               },function(data,status){

               })
       })();



        var viewAppointmentController = function($scope,$modalInstance,appointment){
           $scope.disabled = true;
           $scope.newAppointment = appointment;
           $scope.doctors = null;
            ($scope.getAllDoctors = function(){
                DoctorManager.getAllDoctors(function(err,result){
                    if(err){
                        toastr.error("Cannot get Doctor List:"+err);
                        return;
                    }
                    for(var i = 0; i<result.length;i++){
                        result[i].name = result[i].firstName+" "+result[i].lastName
                    }
                    $scope.doctors = result;
                    console.log(result);
                })
            })()
           $scope.createAppointment = function(){
               AppointmentManager.saveAppointment($scope.newAppointment,$scope.newAppointment.id)
                   .then(function(data,status){
                    toastr.success("Appointment has been updated")
                    $modalInstance.close(true);

                   },function(data,status){
                     toastr.error('Cannot save the appointment')
                   })

           }

            $scope.cancel = function(){
                $modalInstance.dismiss('cancel')
            }

            $scope.deleteAppointment = function(){
                AppointmentManager.deleteAppointment($scope.newAppointment.id)
                    .then(function(data,status){
                        toastr.success("Appointment has been deleted")
                        $modalInstance.close(true);

                    },function(data,status){
                        toastr.error('Cannot save the appointment')
                    })
            }

        };


            (scope.getAllDoctors = function(){
                DoctorManager.getAllDoctors(function(err,result){
                    if(err){
                        toastr.error("Cannot get Doctor List:"+err);
                        return;
                    }
                    for(var i = 0; i<result.length;i++){
                        result[i].name = result[i].firstName+" "+result[i].lastName
                    }
                    scope.doctors = result;
                })
            })() ;

        var newAppointmentController = function($scope,$modalInstance){
            $scope.doctors = null;
            $scope.patients = null;
            ($scope.getAllDoctors = function(){
                DoctorManager.getAllDoctors(function(err,result){
                    if(err){
                        toastr.error("Cannot get Doctor List:"+err);
                        return;
                    }
                    for(var i = 0; i<result.length;i++){
                        result[i].name = result[i].firstName+" "+result[i].lastName
                    }
                    $scope.doctors = result;
                    console.log(result);
                })
            })() ;
            $scope.cancel = function(){
                $modalInstance.dismiss('cancel')
            };
            ($scope.getAllPatients = function(){
                PatientManager.getAllPatients(function(err,result){
                    if(err){
                        toastr.error("Error getting all patients:"+err);
                        return;
                    }
                    for(var i = 0; i<result.length;i++){
                        result[i].name = result[i].firstName+" "+result[i].lastName + " ("+result[i].officialId+")"
                    }
                    $scope.patients = result;
                })
            })();
            $scope.createAppointment = function(){
                AppointmentManager.createNewAppointment($scope.newAppointment)
                    .then(function(data,status){
                        toastr.success("Appointment has been updated")
                        $modalInstance.close(true);

                    },function(data,status){
                        toastr.error('Cannot save the appointment')
                    })
            }
        }



        scope.createAppointment=function(){
            var newAppointmentModal = modal.open({
                templateUrl: 'views/modal/new-appointment-calendar.html',
                controller: newAppointmentController


            });
            newAppointmentModal.result.then(function (selectedItem) {
                //reload the appointments here
                scope.init();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });



        }

        scope.openAppointmentModal = function(calendarObject){
            console.log(calendarObject);
            var calendarModal = modal.open({
                templateUrl: 'views/modal/new-appointment.html',
                controller: viewAppointmentController,
                resolve: {
                    appointment: function () {
                        return calendarObject.bulkObject;
                    }
                }

            });
            calendarModal.result.then(function (selectedItem) {
                //reload the appointments here
                scope.init();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });



        }


        scope.filterChange = function(doctorId){
            if(doctorId=="all"){
               scope.init();
               return;
            }
            //if not then use endpoint to filter the doctor
             AppointmentManager.filterAppointmentByDoctor(doctorId)
                 .then(function(result){
                     var events = []
                     result.data.forEach(function(appointment){
                         var patientName = appointment.patient.firstName + ' ' + appointment.patient.lastName;
                         var doctorName = appointment.doctor.firstName + ' '+ appointment.doctor.lastName;

                         var obj = {
                             bulkObject:appointment,
                             title:patientName+ ' to see ' + doctorName, // The title of the event
                             type: 'info', // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
                             startsAt: appointment.dateTime, // A javascript date object for when the event starts
                             //endsAt: new Date(2014,8,26,15), // Optional - a javascript date object for when the event ends
                             editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable.
                             deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
                             draggable: true, //Allow an event to be dragged and dropped
                             resizable: true, //Allow an event to be resizable
                             incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
                             cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
                         }
                        events.push(obj);

                     })

                     scope.events = [];
                     scope.events = events;
                     console.log(scope.events);

                 },function(result){
                     toastr.error("Cannot get the appointment list"+result.statusText)
                 })

        }



    }])