angular.module('Medirec.controller.pharmacy', ['ngRoute']).
    controller('PharmacyController',['$scope','$location','LoginManager','PatientManager','$modal','PharmacyManager',function(scope,location,LoginManager,PatientManager,modal,PharmacyManager){
         scope.drugs = [];

        (scope.init = function(){
             PharmacyManager.getAllDrugs()
                 .then(function(data,status){
                     console.log(data);
                     scope.drugs = data.data;

                 },function(data,status){
                     toastr.error("Cannot get the drug list:"+status);
                 })
          })()

        var createDrugModalController = function($scope,$modalInstance,drug){
            if(drug==false)
            $scope.drug = {};

            else{
                $scope.drug = angular.copy(drug);
                $scope.drug.active = $scope.drug.active.toString();
                $scope.drug.allowMultipleLots = $scope.drug.allowMultipleLots.toString();
                $scope.drug.allowCombiningLots = $scope.drug.allowCombiningLots.toString();
            }

            $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
            }

            $scope.saveDrug = function(){
                if(!$scope.drug.id){
                    PharmacyManager.createNewDrug($scope.drug)
                        .then(function(data,status){
                            toastr.success("Drug Successfully added to inventory!")
                            $modalInstance.close('done')

                        },function(data,status){
                            toastr.error("Cannot create drug:"+status);
                            console.log(data.data);
                        })
                }
                else{
                    PharmacyManager.updateDrug($scope.drug)
                        .then(function(data,status){
                            toastr.success("Drug Successfully updated!")
                            $modalInstance.close('done')

                        },function(data,status){
                            toastr.error("Cannot update drug:"+status);
                            console.log(data.data);
                        })

                }

            }
        }
          scope.launchCreateDrugModal = function(){

                  var modalInstance = modal.open({
                      templateUrl: 'views/modal/create-drug.html',
                      controller: createDrugModalController,
                      resolve: {
                          drug: function () {
                              return false;
                          }
                      }
                  });

                  modalInstance.result.then(function (selectedItem) {
                      scope.init();
                  }, function () {
                      $log.info('Modal dismissed at: ' + new Date());
                  });
              };

        scope.showDrugInformation = function(drug){
            var modalInstance = modal.open({
                templateUrl: 'views/modal/create-drug.html',
                controller: createDrugModalController,
                resolve: {
                    drug: function () {
                        return drug;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                scope.init();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        }







    }])