angular.module('Medirec.controller.encounter', ['ngRoute']).
    controller('SingleEncounterController',['$scope','$location','DoctorManager','LoginManager','$routeParams','EncounterManager','$modal','PatientManager','PharmacyManager',function(scope,location,DoctorManager,LoginManager,routeParams,EncounterManager,modal,PatientManager,PharmacyManager){
        scope.encounter = {
            customNotes:[]
        };
        (function initialize(){
            var encounterId = routeParams.id;
            scope.encounterDataView = 'basic';
            if (encounterId){
                //get the information from the server
                EncounterManager.getFullEncounter(encounterId)
                    .success(function(result,status){
                        console.log(result)
                        scope.encounter = result.data;
                    }).error(function(data,status){
                        toastr.error("Cannot get encounter from server:"+status);
                    })
            }
        })();

            (scope.getAllDoctors = function(){
                DoctorManager.getAllDoctors(function(err,result){
                    if(err){
                        toastr.error("Cannot get the list of doctors:"+err);
                        return;
                    }
                    console.log(result);
                    scope.doctors = result;
                })

            })();

        var ChooseDateController = function($scope, $modalInstance)
        {
            $scope.selectedDate = null;
            $scope.dateTitle = "Pick A Date";

            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.confirmDate = function(selectedDate)
            {
                console.log(selectedDate)
                $modalInstance.close( selectedDate );
            }
        }

        scope.updateEncounter=function(){
            EncounterManager.updateEncounter(scope.encounter)
                .then(function(result){
                    toastr.success("updated the encounter successfully");

                },function(result){

                })

        }


        var chooseDrugController = function($scope,$modalInstance){
            PharmacyManager.getAllDrugs()
                .then(function(data){
                  $scope.drugs = data.data;

                },function(data){
                    toastr.error("Cannot get pharmacy list:"+data.statusText);
                })

            $scope.selectThis=function(data){
                $modalInstance.close(data);
            }
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

        }

        var newPrescriptionController = function($scope,$modalInstance,patient){
            console.log(patient);
            $scope.patient = patient;
            $scope.prescription={}
            $scope.prescriptions = []

            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.chooseDate = function(isStartDate)
            {
                var datePickerModal = modal.open({
                    templateUrl: 'views/modal/date-picker-modal.html',
                    controller: ChooseDateController

                });

                datePickerModal.result.then(function(date) {
                    console.log(date);
                    $scope.prescription.startingDate = date;
                });

            };




            $scope.searchPharmacy = function(){
                var drugPickerModal = modal.open({
                    templateUrl: 'views/modal/pharmacy-list.html',
                    controller: chooseDrugController

                });

                drugPickerModal.result.then(function(data) {
                    $scope.prescription.drug = data.name;
                });





            };





            //get the list of
                ( $scope.init = function(){
                 PatientManager.getPatientPrescription($scope.patient.id)
                     .then(function(data,status){
                         $scope.prescriptions = data.data;
                         $scope.prescriptions.forEach(function(obj){
                             obj.active = obj.active.toString();
                             obj.subtitutionAllowed = obj.subtitutionAllowed.toString();
                         })
                         console.log(data);

                     },function(data,status){

                     })
             }  )();

            $scope.savePrescription = function(){
                $scope.prescription.encounterId = scope.encounter.id;

                //if there is an id then update
                if(!$scope.prescription.id){
                    PatientManager.createPatientPrescription($scope.patient.id,$scope.prescription)
                        .then(function(data,status){
                            toastr.success("Successful prescription added!");
                            $scope.init();

                        },function(data,status){
                            toastr.error("Cannot  create prescription:"+data.statusText);
                        })
                }
                else{
                    PatientManager.updatePatientPrescription($scope.patient.id,$scope.prescription)
                        .then(function(data,status){
                           toastr.success("Successful update");
                           $scope.init();

                        },function(data,status){
                            toastr.error("Cannot update the prescription:"+status);
                        })



                }

            }
            $scope.showPrescription=function(prescription){
                console.log(prescription)
                $scope.prescription = {};
                $scope.prescription = angular.copy(prescription);
            }
            $scope.createPrescriptionForm = function(){
                $scope.prescription = {};

            }
        }

        scope.openNewPrescriptionModal = function(){
            var addAllModal = modal.open({
                templateUrl: 'views/modal/patient-prescription.html',
                controller: newPrescriptionController,
                size:'lg',
                resolve: {
                    patient: function () {
                        return scope.encounter.patient;
                    }
                }

            });
            addAllModal.result.then(function (selectedItem) {
                initialize();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }


        var newCustomNoteController = function($scope,$modalInstance){
            $scope.label = null
            $scope.create = function(){
                var newObj = {};
                newObj.label = $scope.label;
                newObj.text = '';
                $modalInstance.close(newObj);

            }
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }


        }
        scope.deleteCustomNote=function(indexValue){
            scope.encounter.customNotes.splice(indexValue,0);
        }

        scope.createCustomNote = function(){
            var addAllModal = modal.open({
                templateUrl: 'views/modal/custom-note-create-modal.html',
                controller: newCustomNoteController
            });
            addAllModal.result.then(function (newNote) {
                if (!scope.encounter.customNotes)
                    scope.encounter.customNotes = [];

                scope.encounter.customNotes.push(newNote);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }


        scope.changeEncounterView = function(view){
            scope.encounterDataView = view;

        }


        scope.goToEncounters = function(){
            location.path('/encounters');
        }

        var confirmFinishEncounterController=function($scope,$modalInstance){
               $scope.confirm = function(){
                   $modalInstance.close('confirm');
               }
               $scope.cancel = function(){
                   $modalInstance.dismiss('cancel');
               }
        }

        scope.finishAndSaveEncounter = function(){
            var addAllModal = modal.open({
                templateUrl: 'views/modal/confirm-finish-encounter.html',
                controller: confirmFinishEncounterController
            });
            addAllModal.result.then(function (result) {
               if(result=='confirm'){
                  EncounterManager.closeEncounter(scope.encounter)
                      .then(function(result){
                         alert("success");

                      },function(data){
                           alert("fail")
                      })

               }

                scope.encounter.customNotes.push(newNote);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }


    }])
    .controller('EncountersController',['$scope','$location','DoctorManager','LoginManager','$routeParams','EncounterManager','$modal',function(scope,location,DoctorManager,LoginManager,routeParams,EncounterManager,modal){
        scope.encounters = [];
        function initialize(){
                //get the information from the server
                EncounterManager.getEncountersNotClosed()
                    .success(function(result,status){
                        console.log(result)
                        scope.encounters = result.data;
                    }).error(function(data,status){
                        toastr.error("Cannot get encounter from server:"+status);
                    })
        }
        initialize()

        var changeStatusController = function($scope,$modalInstance,encounter){
            $scope.encounter = angular.copy(encounter);
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }
            $scope.updateStatus = function(){
                EncounterManager.updateEncounter($scope.encounter)
                    .success(function(err,status){
                        toastr.success("Successfully updated the encounter status!")
                        $modalInstance.close('updated');

                    })
                    .error(function(data,status){
                        toastr.error("Cannot update encounter status:"+status);
                        return
                    })
            }
        }

        scope.changeStatus = function(encounter){

            var addAllModal = modal.open({
                templateUrl: 'views/modal/change-status-encounter.html',
                controller: changeStatusController,
                resolve: {
                    encounter: function () {
                        return encounter;
                    }
                }

            });
            addAllModal.result.then(function (selectedItem) {
                initialize();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        }

        var changeDoctorController = function($scope,$modalInstance,encounter){
            $scope.encounter = angular.copy(encounter);
            ($scope.getAllDoctors = function(){
                DoctorManager.getAllDoctors(function(err,result){
                    if(err){
                        toastr.error("Cannot get Doctor List:"+err);
                        return;
                    }
                    for(var i = 0; i<result.length;i++){
                        result[i].name = result[i].firstName+" "+result[i].lastName
                    }
                    $scope.doctors = result;
                    console.log(result);
                })
            })()

            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.updateEncounterDoctor = function(){
                EncounterManager.updateEncounter($scope.encounter)
                    .success(function(err,status){
                        toastr.success("Successfully updated the encounter doctor!")
                        $modalInstance.close('updated');

                    })
                    .error(function(data,status){
                        toastr.error("Cannot update encounter doctor:"+status);
                        return
                    })

            }


        }


        scope.changeDoctor=function(encounter){
            var addAllModal = modal.open({
                templateUrl: 'views/modal/encounter-reassign-doctor.html',
                controller: changeDoctorController,
                resolve: {
                    encounter: function () {
                        return encounter;
                    }
                }

            });
            addAllModal.result.then(function (selectedItem) {
                initialize();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        }

        var deleteEncounterController = function($scope,$modalInstance,encounter){
            $scope.encounter =  angular.copy(encounter);
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.deleteEncounter = function(){
                EncounterManager.deleteEncounter($scope.encounter)
                    .success(function(err,status){
                        toastr.success("Successfully deleted the encounter!")
                        $modalInstance.close('updated');

                    })
                    .error(function(data,status){
                        toastr.error("Cannot delete the encounter:"+status);
                        return
                    })

            }




        }




        scope.deleteEncounter = function(encounter){
            var addAllModal = modal.open({
                templateUrl: 'views/modal/encounter-delete.html',
                controller: deleteEncounterController,
                resolve: {
                    encounter: function () {
                        return encounter;
                    }
                }

            });
            addAllModal.result.then(function (selectedItem) {
                initialize();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });


        }





    }])


