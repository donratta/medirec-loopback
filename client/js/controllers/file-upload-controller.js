angular.module('Medirec.controller.fileUpload', ['ngRoute']).
    controller('FileUploadController',['$scope','$modalInstance','$location','DoctorManager','Upload', '$timeout',function(scope,modalInstance,location,DoctorManager,Upload,timeout){
        scope.uploadFiles = function(file, errFiles) {
            scope.f = file;
            scope.errFile = errFiles && errFiles[0];
            if (file) {
                file.upload = Upload.upload({
                    url: '/backupData/uploadRestoreFile',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    timeout(function () {
                        toastr.success("upload successful");
                        modalInstance.close('cancel')
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                        evt.loaded / evt.total));
                });
            }
        }

    }])