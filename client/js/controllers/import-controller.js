angular.module('Medirec.controller.importData', ['ngRoute']).
    controller('ImportDataController',['$scope','$location','LoginManager','UserManager','HospitalManager','HmoManager','$modal','CompanyManager','PatientManager',function(scope,location,LoginManager,UserManager,HospitalManager,HmoManager,modal,CompanyManager,PatientManager){
        scope.hospital = {};
        (scope.getAllPatients = function(){
            PatientManager.getAllPatients(function(err,data){
                if(err){
                    toastr.error("Cannot get patient records:"+err);
                    return;
                }
                data.forEach(function(patient){
                    patient.name = patient.firstName+" "+patient.lastName;
                })
                scope.patients = data;

            })
        })();




    }])
