angular.module('Medirec.controller.hospitalBilling', ['ngRoute']).
    controller('HospitalBillingController',['$scope','$location','LoginManager','UserManager','EncounterManager','$modal','AppointmentManager','DoctorManager','HospitalBillingManager','PatientManager',function(scope,location,LoginManager,UserManager,EncounterManager,modal,AppointmentManager,DoctorManager,HospitalBillingManager,PatientManager){

        (scope.getHospitalBills = function(){
           HospitalBillingManager.getAllBilling()
               .then(function(result){
                  scope.billings = result.data;
               },function(result){
                   toastr.error("Cannot get the list of billing"+result.statusText);
               })

       })();

        var encounterBillController = function($scope,$modalInstance,bill){
            $scope.bill = angular.copy(bill);
            $scope.patientFinancials = null;
            $scope.tempData = null;
            $scope.disabled = true;
            $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
            };
                ($scope.getPatientFinancials = function(){
                    PatientManager.getPatientFinancial(bill.patient.id)
                        .then(function(result){
                            $scope.patientFinancials = result.data;
                        },function(result){
                            toastr.error("Cannot get the patient financials:"+result.statusText);
                        });
                })();
                $scope.goToEditMode = function(){
                   $scope.disabled = false;
                   $scope.tempData = null;
                   $scope.tempData = angular.copy($scope.bill);
                }
                $scope.cancelEdit = function(){
                    $scope.disabled = true;
                    $scope.bill = null;
                    $scope.bill = angular.copy($scope.tempData);
                }
                $scope.saveBillData = function(){
                    HospitalBillingManager.updateEncounterBilling($scope.bill)
                        .then(function(result){
                            toastr.success("Successfully updated!");
                            $modalInstance.close('cancel');
                        },function(result){
                            toastr.error("Cannot save the encounter billing:"+result.statusText);
                        })
                }
                $scope.calculateTotal = function(){
                    if(!$scope.bill.labCost)
                        $scope.bill.labCost = 0;

                    if(!$scope.bill.otherCost)
                        $scope.bill.otherCost = 0;

                    if(!$scope.bill.pharmacyCost)
                        $scope.bill.pharmacyCost=0;

                    if(!$scope.bill.consultationCost)
                        $scope.bill.consultationCost=0;

                    //calculate the total here
                    $scope.bill.totalCost = $scope.bill.labCost+$scope.bill.otherCost+$scope.bill.pharmacyCost+$scope.bill.consultationCost;

                }
        }

        scope.viewEncounterBill = function(encounterBill){
            var addAllModal = modal.open({
                templateUrl: 'views/modal/view-edit-hospital-billing.html',
                controller: encounterBillController,
                resolve: {
                    bill: function () {
                        return encounterBill;
                    }
                }

            });
            addAllModal.result.then(function (selectedItem) {
                scope.getHospitalBills();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });




        }






    }])

