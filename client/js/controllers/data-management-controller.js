angular.module('Medirec.controller.dataManager', ['ngRoute']).
    controller('DataManagementController',['$scope','$location','LoginManager','UserManager','HospitalManager','MessageManager','DataManager','$route','$modal',function(scope,location,LoginManager,UserManager,HospitalManager,MessageManager,DataManager,route,modal){
        scope.backups = [];

        (scope.getAllBackups = function(){DataManager.getAllBackups()
            .then(function(data){
                scope.backups = data.data;
                console.log(data.data);
            },function(data){
                toastr.error("Cannot get backup list from the database")
            }) })()

        scope.downloadBackup = function(id){
             DataManager.downloadBackup(id)
                 .then(function(data){
                     alert('success');
                 },function(data){
                     alert('error');
                 })
        }

        scope.createNewBackup = function(){
            DataManager.createNewBackup()
                .then(function(data){
                    toastr.success("Backup successfully created");
                    route.reload();

                },function(data){
                    toastr.error("Problem creating backup");

                })

        }

        scope.openRestoreModal=function(){
            var modalInstance = modal.open({
                templateUrl: 'views/modal/upload-restore-modal.html',
                controller: 'FileUploadController'

            });

            modalInstance.result.then(function (selectedItem) {
                route.reload();
            }, function () {
                route.reload();
            });
        };









    }])