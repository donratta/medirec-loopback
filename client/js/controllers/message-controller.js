angular.module('Medirec.controller.message', ['ngRoute']).
    controller('MessageController',['$scope','$location','LoginManager','PatientManager','$modal','DoctorManager','MessageManager','$route',function(scope,location,LoginManager,PatientManager,modal,DoctorManager,MessageManager,route){
        scope.inboxItems = [];
        scope.showMessageDetail = false;


        (scope.getAllInbox = function(){
            MessageManager.getUserInboxMessages(localStorage.getItem('userId'))
                .then(function(data,status){
                    scope.inboxItems = data.data.data;
                    console.log(scope.inboxItems);
                },function(data,status){
                    toastr.error("Cannot get inbox items:"+status);
                    console.log(data);
                })
        })();

        var newMessageModalController = function($scope,$modalInstance){

            $scope.message = {}
            $scope.message.recipients = [];

            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }
            var getDoctorListController = function($scope,$modalInstance){
                $scope.doctors = [];
                DoctorManager.getAllDoctors(function(err,docs){
                    if(err){
                        toastr.error("Cannot get doctor list:"+err);
                        return;
                    }
                    console.log(docs);
                    $scope.doctors = docs;
                })

                $scope.selectDoctor = function(doctor){
                    $modalInstance.close(doctor);
                }
                $scope.close = function(){
                    $modalInstance.dismiss('cancel');
                }


            }

            $scope.addRecipient = function(){
                var addRecipientsModal = modal.open({
                    templateUrl: 'views/modal/add-recipients-doctors.html',
                    controller: getDoctorListController

                });

                addRecipientsModal.result.then(function(recip) {
                     var exists = false
                     for(var count=0;count<$scope.message.recipients.length;count++){
                         if(recip.id == $scope.message.recipients[count].id){
                             exists =  true;
                             break;
                         }

                     }
                    if(!exists){
                        $scope.message.recipients.push(recip);

                    }
                },function(){

                });


            }

            $scope.sendMessage = function(){
                 console.log($scope.message);
                  MessageManager.createMessage($scope.message)
                      .then(function(data,status){
                         toastr.success("Message sent!");
                          $modalInstance.close('sent');
                      },function(data,status){
                         toastr.error("Cannot send message:"+status);

                      })
            }
        }

        scope.setToRead=function(message){
            var obj = angular.copy(message) ;
            delete obj.message;
            obj.isRead = true;
            MessageManager.updateMessageUser(obj)
                .then(function(data){
                    console.log(data.data);
                   //do nothing
                },function(data){
                    toastr.error("Cannot reach server:"+data.statusText);
                })
        }
        scope.openMessageDetails = function(message){
            scope.currentMessage = {}
            scope.currentMessage = angular.copy(message);
            scope.ShowMessageDetail = true;
            if(!message.isRead)
            scope.setToRead(message);
        }
        scope.backToInbox = function(){
           route.reload();

        }



        scope.openNewMailModal = function(){
            var newMessageModal = modal.open({
                templateUrl: 'views/modal/create-message.html',
                controller: newMessageModalController

            });

            newMessageModal.result.then(function(date) {
                scope.getAllInbox();
            },function(){

            });



        }


    }])