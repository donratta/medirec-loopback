angular.module('Medirec.controller.patient', ['ngRoute']).
    controller('PatientController',['$scope','$location','LoginManager','PatientManager',function(scope,location,LoginManager,PatientManager){
        scope.currentUser = {}
        scope.searchData={};
        scope.patients = []
        scope.goToPatient = function(){
            location.path('/patients');
        }
        scope.goToCreatePatient=function(){
            location.path('/patients/create');
        }
        scope.searchPatient=function(){
            var searchParam = scope.searchData.param;
            PatientManager.searchPatient(searchParam)
                .then(function(data,status){
                    console.log(data.data.data);
                    scope.patients = data.data.data;
                },function(data,status){
                     toastr.error("Cannot get search Result:"+data.status)
                })
        }
        scope.getAllPatients = function(){
            PatientManager.getAllPatients(function(err,data){
                if(err){
                    toastr.error("Cannot get patient data:"+err);
                    return;
                }
                scope.patients = data;
            })
        }
        scope.getAge = function(theDate){
            var thisYear = new Date().getFullYear();
            return thisYear- new Date(theDate).getFullYear();
        }

    }])
    .  controller('PatientCreateController',['$scope','$location','LoginManager','$modal','$filter','PatientManager',function(scope,location,LoginManager,modal,filter,PatientManager){
        scope.currentUser = {}
        scope.pictureUrl = '';
        scope.newPatient = {}


        scope.goToPatient = function(){
            location.path('/patients');
        }

        var ChooseDateController = function($scope, $modalInstance)
        {
            $scope.selectedDate = null;
            $scope.dateTitle = "Pick A Date";

            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.confirmDate = function(selectedDate)
            {
                console.log(selectedDate)
                $modalInstance.close( selectedDate );
            }
        }

        scope.displayDOB = null;
        scope.chooseDate = function(isStartDate)
        {
            var datePickerModal = modal.open({
                templateUrl: 'views/modal/date-picker-modal.html',
                controller: ChooseDateController

            });

            datePickerModal.result.then(function(date) {
                console.log(date);
                scope.displayDOB = filter('date')(date, "fullDate");
                scope.newPatient.dob = date;
            });

        }
        scope.createPatientAccount = function(){
            //get the object here and send to DB
            PatientManager.createPatient(scope.newPatient,function(err,data){
                if(err){
                    toastr.error("Error creating account:"+err);
                    result;
                }
                toastr.success("Patient Account Created");
                location.path('/patients/'+data.id);

            })

        }

    }])
    .controller('PatientProfileController',['$scope','$location','LoginManager','$modal','$filter','PatientManager','$routeParams','ImmunizationManager','DoctorManager','HmoManager','CompanyManager','PharmacyManager','UserManager',function(scope,location,LoginManager,modal,filter,PatientManager,routeParams,ImmunizationManager,DoctorManager,HmoManager,CompanyManager,PharmacyManager,UserManager){
        scope.patientAccount = {};
        (function initlialize(){
            var officialId = routeParams.id;
            if (officialId){
                //get the information from the server
                PatientManager.getPatientProfile(officialId,function(err,data){
                    if(err){
                        toastr.error("Cannot get account:"+err);
                    }
                    console.log(data);
                    scope.patientAccount = data;
                    scope.loadAllPatientFinancial();
                    scope.getPatientEncounters();
                    scope.getAllPatientNotes();
                })
            }

            UserManager.loadUserProfile(function(err,result){
                if(err){
                    alert("error getting user profile")
                    return
                }

            })
        })();

        scope.goToPatientEncounters = function(){
            location.path('/patients/'+scope.patientAccount.id+"/encounters");
        }

       scope.financials =[];
       scope.patientEncounters = [];

       scope.loadAllPatientFinancial = function(){
            PatientManager.getPatientFinancial(scope.patientAccount.id)
                .then(function(data){
                    scope.financials = data.data;
                },function(data){
                    toastr.error("Cannot get patients financial:"+data.statusText);
                })

        };



        var chooseDrugController = function($scope,$modalInstance){
            PharmacyManager.getAllDrugs()
                .then(function(data){
                    $scope.drugs = data.data;

                },function(data){
                    toastr.error("Cannot get pharmacy list:"+data.statusText);
                })

            $scope.selectThis=function(data){
                $modalInstance.close(data);
            }
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

        }


           scope.getPatientEncounters = function(){
               PatientManager.getPatientEncounters(scope.patientAccount.id)
                   .then(function(data){
                       console.log(data.data);
                       scope.patientEncounters = data.data;
                   },function(data){
                       toastr.error("Cannot get patient encounters");
                   })
           }


        var patientFinancialModalController = function($scope,$modalInstance){
            $scope.hmos = [];
            $scope.financials = [];
            $scope.financial = null;
            $scope.companies = [];


            $scope.showFinancial = function(data){
                $scope.financial = {}
                $scope.financial = angular.copy(data);

            }
            $scope.clearForm = function(){
                $scope.financial = {};
            }

            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            };
                ($scope.getAllFinancials=function(){
                PatientManager.getPatientFinancial(scope.patientAccount.id)
                    .then(function(data){
                        console.log(data.data);
                        $scope.financials = data.data;
                    },function(data){
                        toastr.error("Cannot get patients financial:"+data.statusText);
                    })
            })();
                    (scope.getAllHmos = function(){
                HmoManager.getAllHmo()
                    .then(function(data){
                        $scope.hmos = data.data

                    },function(data){
                        toastr.error("Cannot get hmo list from the server:"+data.statusText);

                    })
            })();

            (scope.getAllCompanies = function(){
                 CompanyManager.getAllCompanies()
                     .then(function(data){
                         $scope.companies = data.data;

                     },function(data){
                         toastr.error("Error getting list of companies"+data.statusText);
                     })
            } )();



            $scope.savePatientFinancial=function(){
                if($scope.financial.id){
                    PatientManager.updateFinancial(scope.patientAccount.id,$scope.financial)
                        .then(function(data){
                            toastr.success("Update complete");
                            $scope.getAllFinancials();

                        },function(data){
                            toastr.error("Cannot update the insurance record:"+data.statusText)
                        })
                }
                else{
                    PatientManager.createFinancial(scope.patientAccount.id,$scope.financial)
                        .then(function(data){
                            toastr.success("Update complete");
                            $scope.getAllFinancials();

                        },function(data){
                            toastr.error("Cannot update the insurance record:"+data.statusText)
                        })

                }




            }





        }

        scope.LaunchPatientFinancialModal = function(){
            var PatientFinancialModal = modal.open({
                templateUrl: 'views/modal/patient-financial-modal.html',
                controller: patientFinancialModalController
            });




        }





        var newEncounterController = function($scope,$modalInstance){
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.encounter = {
                bmi:0
            };
            $scope.calculateBmi = function(){
                if($scope.encounter.height&&$scope.encounter.weight){
                    $scope.encounter.bmi = ($scope.encounter.weight/(($scope.encounter.height/100)*($scope.encounter.height/100)))
                }
            }

            $scope.doctors = null;
            ($scope.getAllDoctors = function(){
                DoctorManager.getAllDoctors(function(err,result){
                    if(err){
                        toastr.error("Cannot get Doctor List:"+err);
                        return;
                    }
                    for(var i = 0; i<result.length;i++){
                        result[i].name = result[i].firstName+" "+result[i].lastName
                    }
                    $scope.doctors = result;
                    console.log(result);
                })
            })()

            $scope.setLatestGeneralMedicals = function(){
                if(!scope.patientAccount.generalMedical)
                    scope.patientAccount.generalMedical = {};


                scope.patientAccount.generalMedical.weight = $scope.encounter.weight;
                scope.patientAccount.generalMedical.height = $scope.encounter.height;
                PatientManager.savePatient(scope.patientAccount,function(err,result){
                    if(err){
                        toastr.error("Cannot update patients general medical"+err);
                        return;
                    }
                    console.log("Patient data updated");
                    $scope.pushToDoctorWaitList();
                })

            }
            $scope.pushToDoctorWaitList = function(){
                //get the patientID and the doctor ID
                console.log($scope.encounter)
                $scope.encounter.doctorId = $scope.encounter.doctor.id;
                delete $scope.encounter.doctor;
                $scope.encounter.status = "Wait";
                $scope.encounter.createdAt= new Date();
                PatientManager.createNewPatientEncounter(scope.patientAccount.id,$scope.encounter)
                    .success(function(data,status){
                        $modalInstance.dismiss('cancel');
                        toastr.success("Patient Added to wait-list");
                        console.log(data);
                        location.path('/encounters/'+data.id)  ;

                    }).error(function(data,status){
                        toastr.error("Error creating the encounter:"+status);
                        console.log(result)
                    })

                console.log($scope.encounter)
            }


        }


        scope.startEncounter = function(){
            //start new encounter form here
            var addAllModal = modal.open({
                templateUrl: 'views/modal/new-encounter-form.html',
                controller: newEncounterController
            });




        }

        var createAllergyModalController = function($scope, $modalInstance){
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.allergy= {};
            $scope.allergy.dateReported = new Date();

            $scope.saveAllergy = function(){
                //check the validation first
                if(!$scope.allergy.title||$scope.allergy.title==""||!$scope.allergy.status||!$scope.allergy.occurrence||!$scope.allergy.reaction||!$scope.allergy.referredBy){
                    toastr.warning("Data input is invalid");
                    return;
                }
                //if the data is valid then dismiss the modal and pass it back
                $modalInstance.close($scope.allergy);

            }


        }
        var createMedicalProblemModalController = function($scope, $modalInstance){
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.medicalProblem= {};
            $scope.medicalProblem.dateReported = new Date();

            $scope.saveMedicalProblem = function(){
                //check the validation first
                if(!$scope.medicalProblem.title||$scope.medicalProblem.title==""||!$scope.medicalProblem.status||!$scope.medicalProblem.occurrence){
                    toastr.warning("Data input is invalid");
                    return;
                }
                //if the data is valid then dismiss the modal and pass it back
                $modalInstance.close($scope.medicalProblem);
            }
        }
        var ChooseDateController = function($scope, $modalInstance)
        {
            $scope.selectedDate = null;
            $scope.dateTitle = "Pick A Date";

            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.confirmDate = function(selectedDate)
            {
                console.log(selectedDate)
                $modalInstance.close( selectedDate );
            }
        }

        var newImmunizationListController = function($scope, $modalInstance){
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }
            $scope.immunizations = null;
            ImmunizationManager.getAllImmunizations(function(err,result){
                if(err){
                   toastr.err("Cannot get immunization:"+err);
                    return;
                }
                $scope.immunizations = result;

            })

            $scope.selectThisImmunization=function(immunization){
                $modalInstance.close( immunization );

            }

        }

        var newImmunizationController = function($scope, $modalInstance){
            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }
            $scope.immunization= {};


            $scope.setDateGiven = function(){
                var datePickerModal = modal.open({
                    templateUrl: 'views/modal/date-picker-modal.html',
                    controller: ChooseDateController

                });

                datePickerModal.result.then(function(date) {
                    $scope.immunization.dateGivenText = filter('date')(date, "fullDate");
                    $scope.immunization.dateGiven  = date;
                });

            }
            $scope.setDateExpiry=function(){
                var datePickerModal = modal.open({
                    templateUrl: 'views/modal/date-picker-modal.html',
                    controller: ChooseDateController

                });

                datePickerModal.result.then(function(date) {
                    $scope.immunization.dateExpiryText = filter('date')(date, "fullDate");
                    $scope.immunization.dateExpiry = date;
                });

            }
            $scope.saveImmunization = function(){
                //check the validation first
                if(!$scope.immunization.name||$scope.immunization.name==""||!$scope.immunization.dateGiven){
                    toastr.warning("Data input is invalid");
                    return;
                }
                //if the data is valid then dismiss the modal and pass it back
                $modalInstance.close($scope.immunization);
            }

            $scope.setImmunizationFromList = function(){
                var pickImmuneModal = modal.open({
                    templateUrl: 'views/modal/immunization-list.html',
                    controller: newImmunizationListController
                });

                pickImmuneModal.result.then(function(result) {
                    $scope.immunization.name = result.code+"-"+result.name;
                });
            }
        }
        var setGeneralMedicalModalController = function($scope, $modalInstance)
        {
            $scope.patientAccount = scope.patientAccount;


            ($scope.calculateBmi = function(){
                if($scope.patientAccount.generalMedical&&$scope.patientAccount.generalMedical.height&&$scope.patientAccount.generalMedical.weight){
                    $scope.patientAccount.generalMedical.bmi = ($scope.patientAccount.generalMedical.weight/(($scope.patientAccount.generalMedical.height/100)*($scope.patientAccount.generalMedical.height/100)))
                }
            })()



            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.saveGeneralMedical = function(){
                //save the data to the server here
                PatientManager.savePatient($scope.patientAccount,function(err,result){
                    if(err){
                        toastr.warning("Cannot save the patient information:"+err);
                        return;
                    }
                    $modalInstance.close($scope.patientAccount);

                })
            }

            $scope.addAllergyModal = function(){
                var addAllModal = modal.open({
                    templateUrl: 'views/modal/create-patient-allergy.html',
                    controller: createAllergyModalController
                });
                addAllModal.result.then(function(data) {
                    if(!$scope.patientAccount.generalMedical){
                        $scope.patientAccount.generalMedical = {};

                    }
                    if(!$scope.patientAccount.generalMedical.allergies){
                        $scope.patientAccount.generalMedical.allergies = []
                    }
                    $scope.patientAccount.generalMedical.allergies.push(data);
                    console.log(data);
                    //
                });



            }
            $scope.addMedicalProblemModal = function(){
                var addAllModal = modal.open({
                    templateUrl: 'views/modal/create-patient-medical-problem.html',
                    controller: createMedicalProblemModalController
                });
                addAllModal.result.then(function(data) {
                    if(!$scope.patientAccount.generalMedical){
                        $scope.patientAccount.generalMedical = {};

                    }
                    if(!$scope.patientAccount.generalMedical.medicalProblems){
                        $scope.patientAccount.generalMedical.medicalProblems = []
                    }
                    $scope.patientAccount.generalMedical.medicalProblems.push(data);
                    console.log(data);
                    //
                });

            }

            $scope.addNewImmunization = function(){
                var addAllModal = modal.open({
                    templateUrl: 'views/modal/create-patient-immunization.html',
                    controller: newImmunizationController
                });
                addAllModal.result.then(function(data) {
                    if(!$scope.patientAccount.generalMedical){
                        $scope.patientAccount.generalMedical = {};

                    }
                    if(!$scope.patientAccount.generalMedical.immunization){
                        $scope.patientAccount.generalMedical.immunization = []
                    }
                    $scope.patientAccount.generalMedical.immunization.push(data);
                    console.log(data);
                    //
                });

            }





        };


            (scope.getPatientBills = function(){
            var officialId = routeParams.id;
            PatientManager.getPatientBills(officialId)
                .then(function(result){
                  scope.patientBills = result.data;

                },function(result){
                    toastr.error("Cannot get the patient bills"+result.statusText);
                })
        })()



        scope.goToPatient=function(){
            location.path('/patients')

        }
        scope.getAge = function(theDate){
            var thisYear = new Date().getFullYear();
            return thisYear- new Date(theDate).getFullYear();
        }




        scope.openAllergyInformation = function(allergy){

            var showAllergyController = function($scope, $modalInstance){
                $scope.cancel = function()
                {
                    $modalInstance.dismiss('cancel');
                }
                $scope.allergy= allergy;

            }

            var addAllModal = modal.open({
                templateUrl: 'views/modal/view-patient-allergy.html',
                controller: showAllergyController
            });




        }

        scope.launchEditPatientGeneralInformation = function(){
            var setGeneralMedicalModal = modal.open({
                templateUrl: 'views/modal/create-edit-general-medical.html',
                controller: setGeneralMedicalModalController,
                size:'lg'

            });

            setGeneralMedicalModal.result.then(function(data) {
                scope.patientAccount = $.extend(true, {}, data);

            });

        }
        scope.openMedicalProblemInformation = function(medicalProblem){
            var createMedicalProblemModalController = function($scope, $modalInstance){
                $scope.cancel = function()
                {
                    $modalInstance.dismiss('cancel');
                }
                $scope.medicalProblem= medicalProblem;
            }
            var addAllModal = modal.open({
                templateUrl: 'views/modal/view-patient-medical-problem.html',
                controller: createMedicalProblemModalController
            });
        }




        scope.setGeneralMedical=function(){

            var setGeneralMedicalModal = modal.open({
                templateUrl: 'views/modal/create-edit-general-medical.html',
                controller: setGeneralMedicalModalController,
                size:'lg'

            });

            setGeneralMedicalModal.result.then(function(data) {
                scope.patientAccount = $.extend(true, {}, data);

            });

        }


        var createAppointmentModalController = function($scope, $modalInstance,patientAccount,AppointmentManager)
        {
            $scope.patientAccount = patientAccount;
            $scope.newAppointment = {}
            $scope.newAppointment.patientId = $scope.patientAccount.id;
            $scope.doctors = null;
            ($scope.getAllDoctors = function(){
                DoctorManager.getAllDoctors(function(err,result){
                    if(err){
                        toastr.error("Cannot get Doctor List:"+err);
                        return;
                    }
                    for(var i = 0; i<result.length;i++){
                        result[i].name = result[i].firstName+" "+result[i].lastName
                    }
                    $scope.doctors = result;
                    console.log(result);
                })
            })()

            $scope.cancel = function(){
                $modalInstance.dismiss('cancel')
            }
            $scope.createAppointment = function(){

                 console.log($scope.newAppointment);
                AppointmentManager.createNewAppointment($scope.newAppointment)
                    .then(function(data,status){
                        toastr.success("Appointment successfully created!");
                        $scope.cancel();

                    },function(data,status){
                        toastr.error("Error creating appointment:"+status)
                    })
            }






        }

            scope.createAppointmentOpenModal = function(){
            var modalInstance = modal.open({
                templateUrl: 'views/modal/new-appointment.html',
                controller: createAppointmentModalController,
                resolve: {
                    patientAccount: function () {
                        return scope.patientAccount;
                    }
                }


            });

            modalInstance.result.then(function(data) {


            });

        }


        var newPrescriptionController = function($scope,$modalInstance,patient){
            console.log(patient);
            $scope.patient = patient;
            $scope.prescription={}
            $scope.prescriptions = []

            $scope.cancel = function()
            {
                $modalInstance.dismiss('cancel');
            }

            $scope.chooseDate = function(isStartDate)
            {
                var datePickerModal = modal.open({
                    templateUrl: 'views/modal/date-picker-modal.html',
                    controller: ChooseDateController

                });

                datePickerModal.result.then(function(date) {
                    console.log(date);
                    $scope.prescription.startingDate = date;
                });

            };

            $scope.searchPharmacy = function(){
                var drugPickerModal = modal.open({
                    templateUrl: 'views/modal/pharmacy-list.html',
                    controller: chooseDrugController

                });

                drugPickerModal.result.then(function(data) {
                    $scope.prescription.drug = data.name;
                });





            };
            //get the list of
            ( $scope.init = function(){
                PatientManager.getPatientPrescription($scope.patient.id)
                    .then(function(data,status){
                        $scope.prescriptions = data.data;
                        $scope.prescriptions.forEach(function(obj){
                            obj.active = obj.active.toString();
                            obj.subtitutionAllowed = obj.subtitutionAllowed.toString();
                        })
                        console.log(data);

                    },function(data,status){

                    })
            }  )();

            $scope.savePrescription = function(){
                $scope.prescription.encounterId = scope.encounter.id;

                //if there is an id then update
                if(!$scope.prescription.id){
                    PatientManager.createPatientPrescription($scope.patient.id,$scope.prescription)
                        .then(function(data,status){
                            toastr.success("Successful prescription added!");
                            $scope.init();

                        },function(data,status){
                            toastr.error("Cannot  create prescription:"+data.statusText);
                        })
                }
                else{
                    PatientManager.updatePatientPrescription($scope.patient.id,$scope.prescription)
                        .then(function(data,status){
                            toastr.success("Successful update");
                            $scope.init();

                        },function(data,status){
                            toastr.error("Cannot update the prescription:"+status);
                        })



                }

            }
            $scope.showPrescription=function(prescription){
                console.log(prescription)
                $scope.prescription = {};
                $scope.prescription = angular.copy(prescription);
            }
            $scope.createPrescriptionForm = function(){
                $scope.prescription = {};

            }
        }

        scope.openNewPrescriptionModal = function(){
            var addAllModal = modal.open({
                templateUrl: 'views/modal/patient-prescription.html',
                controller: newPrescriptionController,
                size:'lg',
                resolve: {
                    patient: function () {
                        return scope.patientAccount;
                    }
                }

            });
            addAllModal.result.then(function (selectedItem) {
                initialize();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });




        };

           scope.getAllPatientNotes = function(){
            PatientManager.getPatientNotes(scope.patientAccount.id)
                .then(function(data){
                    scope.patientNotes = data.data;
                },function(data){
                    toastr.error('Cannot get patient notes'+data.statusText);
                })
            }

        var patientNoteModalController = function($scope,$modalInstance,patient,userAccount){
            console.log(userAccount)
            $scope.note = {}
            $scope.saveNote = function(){
                $scope.note.noteText = $scope.note.noteText+'-Written by '+userAccount.firstName+','+userAccount.lastName;
                $scope.note.createdAt = new Date();
                PatientManager.createPatientNote(patient.id,$scope.note)
                    .then(function(data){
                       scope.getAllPatientNotes();
                       $modalInstance.close(true);
                    },function(data){
                        toastr.error("Cannot create new patient note"+data.statusText);
                    })
            }
        }

        scope.launchNewPatientNoteModal = function(){


            var addAllModal = modal.open({
                templateUrl: 'views/modal/patient-note.html',
                controller: patientNoteModalController,
                resolve: {
                    patient: function () {
                        return scope.patientAccount;
                    } ,
                    userAccount:function(){
                        return UserManager.getProfile();
                    }
                }

            });
            addAllModal.result.then(function (selectedItem) {
                initialize();
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });




        }



    }])

    .  controller('PatientEncounterHistoryController',['$scope','$location','LoginManager','$modal','$filter','PatientManager','$routeParams',function(scope,location,LoginManager,modal,filter,PatientManager,routeParams){

        var officialId = routeParams.id;
        scope.encounters = [];
        //get the encounters here
        (scope.init = function(){
             PatientManager.getPatientEncounters(officialId)
                 .then(function(data){
                    console.log(data.data);
                    scope.encounters = data.data;
                 },function(data){
                     toastr.error("Cannot get patient encounters");
                 })
        })();














    }])
