angular.module('Medirec.controller.hospital', ['ngRoute']).
    controller('HospitalSettingController',['$scope','$location','LoginManager','UserManager','HospitalManager','HmoManager','$modal','CompanyManager',function(scope,location,LoginManager,UserManager,HospitalManager,HmoManager,modal,CompanyManager){
       scope.hospital = {};
       scope.tempData = {}
       scope.settingsView = 'hospital';
       scope.companies = [];
        //get the hospital setting
        scope.tempData = null;
        scope.editHospital = function(){
            scope.editMode = true;
            scope.tempData = angular.copy(scope.hospital);
        };
        scope.cancel = function(){
           scope.hospital = {};
           scope.hospital = angular.copy(scope.tempData);
        };


            (scope.getHospitalSetting = function(){
            HospitalManager.getHospitalSettings()
            .then(function(data){
                 scope.hospital = data.data.data;
                 console.log(data)

            },function(data,status){
                toastr.error("Cannot get hospital setting:"+data.statusText);
            })
        })();


        scope.saveHospital = function(){
            HospitalManager.saveHospital(scope.hospital)
                .then(function(data){
                   toastr.success("Hospital Information Saved")
                   scope.editMode = false;

                },function(data){


                })
        };

        scope.hmos=[];
        (scope.getAllHmos=function(){
            HmoManager.getAllHmo().
                then(function(data){
                    scope.hmos = data.data;
                },function(data){
                    toastr.error('cannot load hmos');
                })

        })();

        var newHmoModalController = function($scope,$modalInstance){

            $scope.createHmo = function(){
                HmoManager.createHmo($scope.newHmo)
                    .then(function(data){
                       toastr.success("Created HMO successfully!")
                       scope.getAllHmos();
                    },function(data){
                        toastr.error("Cannot create new HMO"+data.statusText);
                    })
            }
            $scope.cancel=function(){
                $modalInstance.dismiss('cancel');
            }
        }

        scope.openNewHmoModal=function(){
            var modalInstance = modal.open({
                templateUrl: 'views/modal/create-hmo-modal.html',
                controller:newHmoModalController ,
                resolve: {
                    items: function () {
                        return false;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }


        var showHmoPatientsController = function($scope,$modalInstance,hmo){
            $scope.cancel=function(){
                $modalInstance.dismiss('cancel');
            }

              $scope.hmo = hmo;
              $scope.patients = []
              HmoManager.getPatientsInHmo(hmo.id)
                  .then(function(data){
                     $scope.patients = data.data;
                  },function(data){
                      toastr.error("Cannot get patients registered to this HMO"+data.statusText)
                  })
        }

        scope.getHmoPatients = function(hmo){
            var modalInstance = modal.open({
                templateUrl: 'views/modal/show-hmo-patients-modal.html',
                controller:showHmoPatientsController ,
                resolve: {
                    hmo: function () {
                        return hmo;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
            }, function () {
            });
        };
        (scope.getAllCompanies=function(){
            CompanyManager.getAllCompanies()
                .then(function(data){
                   scope.companies = data.data;
                },function(data){
                    toastr.error("Cannot get list of companies"+data.statusText);
                })
        })()


        var newCompanyModalController = function($scope,$modalInstance){
            $scope.newCompany = {};
            $scope.createCompany = function(){
                CompanyManager.createCompany($scope.newCompany)
                    .then(function(data){
                        toastr.success("Successfully created!");
                        scope.getAllCompanies();
                        $scope.cancel()
                    },function(data){
                        toastr.error("Cannot create new company"+data.statusText);
                    })
            }

            $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
            }


        }

        scope.openNewCompanyModal = function(){
            var modalInstance = modal.open({
                templateUrl: 'views/modal/create-company-modal.html',
                controller:newCompanyModalController ,
                resolve: {
                    items: function () {
                        return false;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });



        }



    }])