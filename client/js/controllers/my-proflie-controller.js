angular.module('Medirec.controller.myProfile', ['ngRoute']).
    controller('MyProfileController',['$scope','$location','DoctorManager','LoginManager','$routeParams','EncounterManager','$modal','PatientManager','PharmacyManager','UserManager',function(scope,location,DoctorManager,LoginManager,routeParams,EncounterManager,modal,PatientManager,PharmacyManager,UserManager){
        scope.profile = {};
        scope.editing = false;
        scope.temporaryProfile = {};
        (scope.init=function(){
            var role =   localStorage.getItem('role');
                UserManager.loadUserProfile(function(err,data){
                    if(err){
                        toastr.error("Cannot load the user data");
                        return;
                    }
                    scope.profile = data;

                }) ;
           })();
        scope.editAccount = function(){
            scope.temporaryProfile = angular.copy(scope.profile);
            scope.editing = true;
        }
        scope.cancelEdit = function(){
            scope.profile = {};
            scope.profile = angular.copy(scope.temporaryProfile);
            scope.editing = false;
        }
        scope.saveAccount = function(){
            UserManager.saveProfile(scope.profile)
                .then(function(data){
                    toastr.success("User profile updated!");
                    console.log(data.data);
                    scope.profile = data.data;
                    scope.editing = false;

                },function(data){
                    toastr.error("Error updating user profile"+data.statusText);
                })
        }

    }])