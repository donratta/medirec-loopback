angular.module('Medirec.controller.login', ['ngRoute']).
    controller('LoginController',['$scope','$location','LoginManager','$http','HospitalManager',function(scope,location,LoginManager,http,HospitalManager){
            scope.crdentials = {}  ;
            scope.hospital = {};
        (scope.getHospitalSetting = function(){
            HospitalManager.getHospitalSettings()
                .then(function(data){
                    scope.hospital = data.data.data;
                },function(data,status){
                    toastr.error("Cannot get hospital setting:"+data.statusText);
                })
        })();




            scope.loginUser = function(){
                //validate the user here
                //TODO - create a dropdown so we can sign in as a doctor nurese or admin here
                if(scope.credentials.role=="doctors"){
                    LoginManager.LoginDoctor(scope.credentials,function(err,data){
                        if(err){
                            if(err==401){
                                toastr.error("Bad Credentials");
                                return
                            }
                            toastr.error("Error Logging In:"+err);
                            return;
                        }
                        //set the token here
                        http.defaults.headers.common['Authorization'] =  data.id;
                        localStorage.setItem("Authorization",data.id)
                        localStorage.setItem("userId",data.userId);
                        localStorage.setItem("role",scope.credentials.role)
                        //go to the home page
                        location.path('/home')


                    });
                }
                else if(scope.credentials.role=="nurses")  {
                    LoginManager.loginNurse(scope.credentials,function(err,data){
                        if(err){
                            if(err==401){
                                toastr.error("Bad Credentials");
                                return
                            }
                            toastr.error("Error Logging In:"+err);
                            return;
                        }
                        //set the token here
                        http.defaults.headers.common['Authorization'] =  data.id;
                        localStorage.setItem("Authorization",data.id)
                        localStorage.setItem("userId",data.userId);
                        localStorage.setItem("role",scope.credentials.role)
                        //go to the home page
                        location.path('/home')
                    });
                }
                else if(scope.credentials.role=="admins")  {
                    LoginManager.loginAdmin(scope.credentials)
                        .then(function(data){
                            console.log(data)
                            http.defaults.headers.common['Authorization'] =  data.data.id;
                            localStorage.setItem("Authorization",data.data.id)
                            localStorage.setItem("userId",data.data.userId);
                            localStorage.setItem("role",scope.credentials.role);
                            location.path('/home')
                        },function(data){
                            if(data.statusCode==401){
                                toastr.error("Bad Credentials");
                                return
                            }
                            toastr.error("Error Logging In:"+data.statusText);

                        })
                        //set the token here

                        //go to the home page
                }

            }

    }])