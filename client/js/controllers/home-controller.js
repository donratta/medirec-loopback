angular.module('Medirec.controller.home', ['ngRoute']).
    controller('HomeController',['$scope','$location','LoginManager','UserManager','EncounterManager','$modal','AppointmentManager','DoctorManager',function(scope,location,LoginManager,UserManager,EncounterManager,modal,AppointmentManager,DoctorManager){
        scope.currentUser = {}
        scope.encountersWaiting = [];
        scope.role =  localStorage.getItem('role');

        scope.goToPatient = function(){
            location.path('/patient');
        }
        UserManager.loadUserProfile(function(err,data){
            if(err){
                toastr.error("Cannot load the user data");
                return;
            }
            scope.currentUser = data;
            UserManager.setProfile(data);
        })


        if(scope.role=='doctors'){
            EncounterManager.getEncountersForDoctorWaiting()
                .then(function(result,status){
                    scope.encountersWaiting = result.data.data;

                },function(data,status){
                    toastr.error("Cannot load the encounters:"+status);
                })

            scope.getDoctor = function(){
                var userId = localStorage.getItem('userId');
                DoctorManager.getDoctorAppointment(userId)
                    .then(function(data,status){
                        scope.appointments = data.data.data;
                    },function(data,status){
                        toastr.error("Cannot get appointment list:"+status);
                    });

            };
            scope.getDoctor();

        }
        else if(scope.role=='nurses'){
            EncounterManager.getEncountersNotClosed()
                .then(function(result){
                    console.log(result.data);
                    scope.patientWaitList = result.data.data;
                },function(result){
                    toastr.error("Cannot get the waitlist for the hospital:"+result.statusText);
                })


            //get all appointments moving forward
            AppointmentManager.getUpcomingAppointments().
                then(function(result){
                   console.log(result.data);
                   scope.appointments = result.data.data;
                },function(result){
                    toastr.error('Cannot get upcoming appointments:'+result.statusText);
                })
        }




        //get the encounters for the doctor that are on the waiting list



        var seePatientController = function($scope,$modalInstance,patient){
            $scope.patient = patient.patient;

            $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
            }

            $scope.patient.status = "withDoctor"

            $scope.seePatient = function(){
                EncounterManager.updateEncounter(patient)
                    .then(function(data,status){
                        $modalInstance.close(patient);

                    },function(data,status){
                        toastr.error("Cannot update encounter status:"+status);
                    })
            }



        }

        scope.seePatient = function(patient){
            var addAllModal = modal.open({
                templateUrl: 'views/modal/warning-see-patient-doctor.html',
                controller: seePatientController,
                resolve: {
                    patient: function () {
                        return patient;
                    }
                }

            });
            addAllModal.result.then(function (selectedItem) {
                location.path('/encounters/'+selectedItem.id);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        }

        scope.goToPatients = function(){
            location.path('/patients')
        };
        scope.appointments = null;



        var  showAppointmentController = function($scope,$modalInstance,appointment){
            $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
            }
            $scope.appointment = appointment;


        }

        scope.showAppointmentInformation = function(appointment){
            var addAllModal = modal.open({
                templateUrl: 'views/modal/show-doctor-appointment.html',
                controller: showAppointmentController,
                resolve: {
                    appointment: function () {
                        return appointment;
                    }
                }

            });
            addAllModal.result.then(function (selectedItem) {
                location.path('/encounters/'+selectedItem.id);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        }





    }])