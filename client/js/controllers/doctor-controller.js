angular.module('Medirec.controller.doctor', ['ngRoute']).
    controller('DoctorController',['$scope','$location','DoctorManager','LoginManager',function(scope,location,DoctorManager,LoginManager){
        scope.currentUser = {}
        scope.searchData={};
        scope.doctors = [];
        scope.goToPatient = function(){
            location.path('/patients');
        }
        scope.goToCreateDoctor=function(){
            location.path('/officials/create');
        }
       scope.getAllDoctors = function(){
          DoctorManager.getAllDoctors(function(err,data){
              if(err){
                  toastr.error("Cannot get all doctors from the server");
                  return;
              }
              console.log(data);
              scope.doctors = data;
          })
        }
    }])
    .controller('DoctorCreateController',['$scope','$location','DoctorManager','LoginManager',function(scope,location,DoctorManager,LoginManager){
        scope.newDoctor = {};

         scope.goToDoctors = function(){
             location.path('/officials');
         }
         scope.createOfficial = function(){
             if(scope.newDoctor.password!=scope.newDoctor.confirmPassword){
                 toastr.error("Password mis-match");
                 return;
             }
             delete scope.newDoctor.confirmPassword;


           if(scope.newDoctor.role=='nurse'){
               scope.newDoctor.status = true;
               scope.newDoctor.password= "password";
               DoctorManager.createNurse(scope.newDoctor)
                   .then(function(data){
                       toastr.success("Successfully created nurse");
                       location.path('/officials');

                   },function(data){
                       toastr.error("Error creating nurse:"+data.statusText);
                   })


           }
             if(scope.newDoctor.role=='doctor'){
                 scope.newDoctor.status = true;

                 DoctorManager.createDoctor(scope.newDoctor)
                     .then(function(data){
                         toastr.success("Successfully created doctor");
                         location.path('/officials');

                     },function(data){
                         toastr.error("Error creating doctor:"+data.statusText);
                         location.path('/officials');

                     })

           }
             if(scope.newDoctor.role=='admin'){
                 scope.newDoctor.status = true;
                 scope.newDoctor.password= "password";
                 DoctorManager.createAdmin(scope.newDoctor)
                     .then(function(data){
                         toastr.success("Successfully created doctor");
                         location.path('/officials');
                     },function(data){
                         toastr.error("Error creating doctor:"+data.statusText);
                     })

           }
         }

    }])


