'use strict';

// Declare app level module which depends on filters, and services
angular.module('Medirec', ['ngRoute', 'ui.bootstrap', 'ngFileUpload',
        'ui.bootstrap.datetimepicker',
        'Medirec.filters.commonFilter',
        'Medirec.services.user',
        'Medirec.services.http',
        'Medirec.services.login',
        'Medirec.services.doctor',
        'Medirec.services.patient',
        'Medirec.services.message',
        'Medirec.services.appointment',
        'Medirec.services.hospital',
        'Medirec.controller.home',
        'Medirec.services.hospitalBilling',
        'mwl.calendar',
        'ui.bootstrap',
        'Medirec.directives.common',
        'Medirec.controller.dataManager',
        'Medirec.controller.login',
        'Medirec.controller.patient',
        'Medirec.controller.fileUpload',
        'Medirec.controller.banner',
        'Medirec.controller.doctor',
        'Medirec.controller.importData',
        'Medirec.services.immunization',
        'Medirec.services.encounter',
        'Medirec.services.dataManager',
        'Medirec.services.pharmacy',
        'Medirec.controller.myProfile',
        'Medirec.services.hmo',
        'Medirec.services.company',
        'Medirec.controller.encounter',
        'Medirec.controller.hospital',
        'Medirec.controller.message',
        'Medirec.controller.pharmacy',
        'Medirec.controller.hospitalBilling',
        'Medirec.controller.appointment',
        'Medirec.controller.sideMenu']).
    config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider) {
        $routeProvider.when('/', {templateUrl: 'views/home.html'});
        $routeProvider.when('/login', {templateUrl: 'views/login.html'});
        $routeProvider.when('/home', {templateUrl: 'views/home.html'});
        $routeProvider.when('/officials', {templateUrl: 'views/doctor.html'});
        $routeProvider.when('/patients', {templateUrl: 'views/patient.html'});
        $routeProvider.when('/patients/create', {templateUrl: 'views/patient-create.html'});
        $routeProvider.when('/patients/:id', {templateUrl: 'views/patient-profile.html'});
        $routeProvider.when('/patients/:id/encounters', {templateUrl: 'views/patient-encounters.html'});
        $routeProvider.when('/officials/create', {templateUrl: 'views/doctor-create.html'});
        $routeProvider.when('/encounters', {templateUrl: 'views/encounters.html'});
        $routeProvider.when('/encounters/:id', {templateUrl: 'views/patient-single-encounter.html'});
        $routeProvider.when('/hospitalSettings', {templateUrl: 'views/hospital-setting.html'});
        $routeProvider.when('/messages', {templateUrl: 'views/messages.html'});
        $routeProvider.when('/appointments', {templateUrl: 'views/appointments.html'});
        $routeProvider.when('/pharmacy', {templateUrl: 'views/pharmacy.html'});
        $routeProvider.when('/dataManagement', {templateUrl: 'views/data-management.html'});
        $routeProvider.when('/myProfile', {templateUrl: 'views/my-profile.html'});
        $routeProvider.when('/billing', {templateUrl: 'views/hospital-billing.html'});
        $routeProvider.when('/import-data', {templateUrl: 'views/import-data.html'});
        $routeProvider.otherwise('/home');
    }]).config([ '$httpProvider',function($httpProvider) {
        $httpProvider.interceptors.push('myHttpResponseInterceptor');



    }])
.run(['$rootScope','$location','$http',function($rootScope,$location,http){
        http.defaults.headers.common['Authorization'] = localStorage.getItem('Authorization');
        $rootScope.$on('$routeChangeStart', function(evt, absNewUrl, absOldUrl){
            if (!localStorage.getItem('Authorization')) {
                if ($location.path() != "/login") {
                    $location.path("/login");
                }
            }
            else{
                http.defaults.headers.common['Authorization'] = localStorage.getItem('Authorization');
            }
        });


    }])