angular.module('Medirec.services.pharmacy', []).



    factory('PharmacyManager', ['$http','UserManager', function(http,UserManager)
    {

        return{
            createNewDrug:function(drugObject){
                return http.post('/api/drugs',angular.toJson(drugObject));
            },
            getAllDrugs:function(){
                return http.get('/api/drugs')
            },
            updateDrug:function(drugObject){
                return http.put('/api/drugs',angular.toJson(drugObject));
            }

        }




    }])