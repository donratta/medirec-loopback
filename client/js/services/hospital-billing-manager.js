angular.module('Medirec.services.hospitalBilling', []).



    factory('HospitalBillingManager', ['$http','UserManager', function(http,UserManager)
    {

        return {

            getAllBilling:function(){
                return http.get('/api/encounterBillings?filter[include]=patient&filter[include]=encounter')
            },
            updateEncounterBilling:function(data){
                return http.put('/api/encounterBillings',angular.toJson(data));
            }

        }
    }])
