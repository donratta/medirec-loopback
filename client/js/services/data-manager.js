angular.module('Medirec.services.dataManager', []).



    factory('DataManager', ['$http','UserManager', function(http,UserManager)
    {
        return{
            getAllBackups:function(){
                return http.get('/backupData/getLatestBackup')
            }
            ,downloadBackup:function(id){
                return http.get('/backupData/downloadBackup/'+id);
            },
            createNewBackup:function(){
                return http.get('/backupData/storeBackup')
            }



        }



    }])
