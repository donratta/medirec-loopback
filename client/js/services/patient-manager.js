angular.module('Medirec.services.patient', []).



    factory('PatientManager', ['$http','UserManager', function(http,UserManager)
    {
        return {
            getAllPatients:function(callback){
                http.get('/api/patients/')
                    .success(function(data,status){
                        callback(null,data);
                    }).error(function(data,status){
                        callback(status,data);
                    })
            },
            createPatient:function(patientObject,callback){
                http.post('/api/patients/',angular.toJson(patientObject))
                    .success(function(data,status){
                        callback(null,data);
                    })
                    .error(function(data,status){
                        console.log(data);
                        callback(status,data);
                    })
            },
            getPatientProfile:function(id,callback){
                http.get('/api/patients/'+id)
                    .success(function(data,status){
                         callback(null,data);
                    })
                    .error(function(data,status){
                         callback(status,data);
                    })
            },
            savePatient:function(patientObject,callback){
                http.put('/api/patients/'+patientObject.id,angular.toJson(patientObject))
                    .success(function(data,status){
                        callback(null,data);
                    })
                    .error(function(data,status){
                         callback(status,data);
                    })
            },
            searchPatient:function(param){
              return http.get('/api/patients/searchRecord?searchParam='+param);
            },
            createNewPatientEncounter:function(patientId,encounterObject){
                return http.post('/api/patients/'+patientId+'/encounters',angular.toJson(encounterObject))

            }
            ,getPatientPrescription:function(patientId){
                return http.get('/api/patients/'+patientId+'/prescriptions')
            },
            createPatientPrescription:function(patientId,prescriptionObject){
                return http.post('/api/patients/'+patientId+'/prescriptions',angular.toJson(prescriptionObject))
            } ,
            updatePatientPrescription:function(patientId,prescriptionObject){
                return http.put('/api/prescriptions',angular.toJson(prescriptionObject));
            } ,
            getPatientEncounters:function(id){
                return http.get('/api/patients/'+id+'/encounters?filter[include]=doctor&filter[include]=patient')
            },
            getPatientFinancial:function(id){
                return http.get('/api/patients/'+id+'/patientFinancials?filter[include]=hmo&filter[include]=company')
            },
            updateFinancial:function(id,financialObject){
                return http.put('/api/patients/'+id+"/patientFinancials/"+financialObject.id,angular.toJson(financialObject));
            },
            createFinancial:function(id,financialObject){
                return http.post('/api/patients/'+id+"/patientFinancials",angular.toJson(financialObject))
            } ,
            createPatientNote:function(id,patientNoteObject){
                return http.post('/api/patients/'+id+'/patientNotes',angular.toJson(patientNoteObject));
            },
            getPatientNotes:function(id){
                return http.get('/api/patients/'+id+'/patientNotes')
            },
            getPatientBills:function(id){
                return http.get('/api/encounterBillings?filter[where][patientId]='+id+'&filter[include]=patientFinancial');
            }

        }












    }])