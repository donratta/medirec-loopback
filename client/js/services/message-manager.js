angular.module('Medirec.services.message', []).



    factory('MessageManager', ['$http','UserManager', function(http,UserManager)
    {
       return{

           createMessage:function(message){
               message.sender = localStorage.getItem('userId');
               if(!message.sender || localStorage.getItem('role')!='doctors'){
                   toastr.error('Not Authorized');
                   return;
               }
               return http.post('/api/messages/sendMessage',angular.toJson(message))
           },
           getUserInboxMessages:function(id){
               return http.get('/api/messages/getMyMessages',{params:{id:id}})
           }   ,
           updateMessageUser:function(message){
               return http.put('/api/message_users',angular.toJson(message));
           }

       }

    }])