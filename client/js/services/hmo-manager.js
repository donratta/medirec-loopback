angular.module('Medirec.services.hmo', []).



    factory('HmoManager', ['$http','UserManager', function(http,UserManager)
    {


        return {
           getAllHmo:function(){
               return http.get('/api/hmos')
           },
           createHmo:function(hmoObject){
               return http.post('/api/hmos',angular.toJson(hmoObject))
           } ,
            getPatientsInHmo:function(id){
                return http.get('/api/hmos/'+id+'/patientFinancials?filter[include]=patient')
            }

        }












    }])