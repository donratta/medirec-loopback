angular.module('Medirec.services.company', []).



    factory('CompanyManager', ['$http','UserManager', function(http,UserManager)
    {

    return{
        createCompany:function(companyObject){
            return http.post('/api/companies',angular.toJson(companyObject));
        },
        getAllCompanies:function(){
            return http.get('/api/companies')
        }

    }



    }])