angular.module('Medirec.services.appointment', []).



    factory('AppointmentManager', ['$http','UserManager', function(http,UserManager)
    {
        return {
            createNewAppointment:function(appointment){
                return http.post('/api/appointments',angular.toJson(appointment))
            },
            getAllAppointments:function(){
                return http.get('/api/appointments')
            },
            getAllAppointmentsWithInfo:function(){
               return http.get('/api/appointments/getAppointmentWithInfo')
            }
            ,saveAppointment:function(appointment,id){
                return http.put('/api/appointments/'+id,angular.toJson(appointment))
            }  ,
            deleteAppointment:function(id){
                return http.delete('/api/appointments/'+id);
            }  ,
            filterAppointmentByDoctor:function(doctorId){
                return http.get('/api/appointments?filter[where][doctorId]='+doctorId+'&filter[include]=doctor&filter[include]=patient');
            },
            getUpcomingAppointments:function(){
                return http.get('/api/appointments/getAllUpcomingAppointments')
            }

        }












    }])