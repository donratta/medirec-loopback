angular.module('Medirec.services.encounter', []).



    factory('EncounterManager', ['$http','UserManager', function(http,UserManager)
    {
        return {
            getAllEncounters:function(callback){
                http.get('/api/encounters/')
                    .success(function(data,status){
                        callback(null,data);
                    }).error(function(data,status){
                        callback(status,data);
                    })
            },
            createEncounter:function(callback){

            }
            ,getEncounter:function(id){
                return http.get('/api/encounters/'+id);
            },
            getFullEncounter:function(id){
                 return http.get('/api/encounters/getFullEncounter/?id='+id);

            } ,
            getEncountersNotClosed:function(){
                return http.get('/api/encounters/getEncountersNotClosed');

            },
            updateEncounter:function(encounter){
                return http.put('/api/encounters/'+encounter.id,angular.toJson(encounter));
            },
            deleteEncounter:function(encounter){
                return http.delete('/api/encounters/'+encounter.id);
            },
            getEncountersForDoctorWaiting:function(){
                return http.get('/api/encounters/getEncountersForDoctorWaiting/?id='+localStorage.getItem("userId"));
            },
            closeEncounter:function(encounter){
                return http.post('/api/encounters/closeEncounter',angular.toJson(encounter));
            }




        }












    }])