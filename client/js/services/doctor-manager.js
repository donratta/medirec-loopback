angular.module('Medirec.services.doctor', []).



    factory('DoctorManager', ['$http','UserManager', function(http,UserManager)
    {
        return {
            getAllDoctors:function(callback){
                http.get('/api/doctors/')
                    .success(function(data,status){
                        callback(null,data);
                    }).error(function(data,status){
                        callback(status,data);
                    })
            },
            createDoctor:function(doctorObject){
                return http.post('/api/doctors',angular.toJson(doctorObject))
            },
            getDoctorAppointment:function(id){
                return http.get("/api/appointments/getDoctorAppointmentsUpcoming?id="+id);
            }  ,
            createNurse:function(nurseObject){

                return http.post('/api/nurses',angular.toJson(nurseObject))
            } ,
            createAdmin:function(adminObject){
               return http.post('/api/admins',angular.toJson(adminObject))
            }

        }












    }])