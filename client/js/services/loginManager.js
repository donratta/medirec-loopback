 angular.module('Medirec.services.login', []).



factory('LoginManager', ['$http','UserManager', function(http,UserManager)
{
    return {
        LoginDoctor:function(credential,callback){
            console.log(credential);
             http.post('/api/doctors/login',angular.toJson(credential))
                 .success(function(data,status){
                     console.log(data);
                     //set the local storage here
                     UserManager.setRole(localStorage.getItem("role"))
                     UserManager.setProfile(data)

                     callback(null,data);
                 })
                 .error(function(data,status){
                     callback(status,data);
                 })
            },
        loginNurse:function(credential,callback){
            console.log(credential);
            http.post('/api/nurses/login',angular.toJson(credential))
                .success(function(data,status){
                    console.log(data);
                    //set the local storage here

                    callback(null,data);
                })
                .error(function(data,status){
                    callback(status,data);
                })
        },
        loginAdmin:function(credential){
            return http.post('/api/admins/login',angular.toJson(credential))
        }
    }












}])