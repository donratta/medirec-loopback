angular.module('Medirec.services.immunization', []).



    factory('ImmunizationManager', ['$http','UserManager', function(http,UserManager)
    {
        return {
            getAllImmunizations:function(callback){
                http.get('/api/immunizations/')
                    .success(function(data,status){
                        callback(null,data);
                    }).error(function(data,status){
                        callback(status,data);
                    })
            }


        }


    }])