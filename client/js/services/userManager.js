angular.module('Medirec.services.user', []).



    factory('UserManager', ['$http', function(http)
    {

        var userProfile = {}
        var userRole = null;


        return{
            setProfile:function(profile){
                userProfile = profile;
            },
            getProfile:function(){
                return userProfile;
            },
            setRole:function(role){
                userRole = role;
            },
            getRole:function(){
                return userRole;
            },
            loadUserProfile:function(callback){
                http.get('/api/'+localStorage.getItem("role")+'/'+localStorage.getItem("userId"))
                .success(function(data,status){
                   userProfile = data;
                   callback(null,data);
                })
                    .error(function(data,status){
                    callback(status,data);
                    })


            },
            saveProfile:function(userObject){
                return http.put('/api/'+localStorage.getItem("role")+'/'+localStorage.getItem("userId"),angular.toJson(userObject));

            }





        }



    }])

