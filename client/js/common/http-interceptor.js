'use strict';


var medirec = angular.module('Medirec.services.http', [])


//Works for 1.1.x versions. 1.0.x is similar and can be figured out using code comments
    medirec.factory('myHttpResponseInterceptor', [ '$q', '$injector', '$window', '$location', '$rootScope', '$filter',
    function($q, $injector, $window, $location, $rootScope, $filter) {
        var latestPayload = null;
        return {

            request: function(config) {
                //console.log(config)

                return config || $q.when(config);
            },

            response : function(response) {
                var headers = response.headers();
                return response;
            },

            responseError: function(responseError) {
                if (responseError.status == 401) {
                    if($location.path()!="/login"){
                        localStorage.removeItem("Authorization");
                        localStorage.removeItem("userId");
                        toastr.error("User has to Sign in");
                        $location.path('/login');
                    }
                }

                return $q.reject(responseError);
            }

        }
    }
]);