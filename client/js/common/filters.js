var medirec = angular.module('Medirec.filters.commonFilter', [])
    .filter('fullName', function() {
        return function(string) {
            console.log(string)
            return string.charAt(0).toUpperCase() + string.slice(1);
        };
    })
    .filter('shortGender',function(){
        return function(input){
            if(input=="male"){
                return "M";
            }
            return "F";
        }
    })
