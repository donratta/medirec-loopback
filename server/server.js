var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    console.log('Web server listening at: %s', app.get('url'));
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
bootOptions = { "appRootDir": __dirname,
    "bootScripts" : [ "./boot/script/roles-script.js","./boot/script/script.js","./boot/script/hmo-script.js","./boot/script/states-script.js","./boot/script/backup-route.js"] };
boot(app,bootOptions, function(err) {
  if (err) throw err;
  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
