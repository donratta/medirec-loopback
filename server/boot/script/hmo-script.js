var fs = require('fs');
module.exports = function(app){
    var Hmo = app.models.Hmo;

    var HMOS = [
        {
            name:"HYGEIA HMO LIMITED",
            address: "Bank of Industry Building BOI. 2nd Floor 21/22 Marina Lagos",
            phone:"08120910005",
            zone:"National",
            code:1
        }, {
            name:"TOTAL HEALTH TRUST LIMITED",
            address: "2 Marconi Road Palmgrove Estate Lagos",
            phone:"07090858202",
            zone:"National",
            code:2
        },
        {
            name:"CLEARLINE INTERNATIONAL LIMITED",
            address: "16 Oyefeso Avenue Off Ikorodu Road Obanikoro Lagos",
            phone:"08052408604",
            zone:"National",
            code:3
        },
        {
            name:"HEALTHCARE INTERNATIONAL LIMITED",
            address: "308A Murtala Mohammed Way, Yaba, Lagos",
            phone:"08052099094-99",
            zone:"National",
            code:4
        },
        {
            name:"Medi Plan Health Care Limited",
            address: "Plot 32b, Admiralty Way, Lekki Phase I,Victoria Island, Lagos.",
            phone:"07055502580",
            zone:"National",
            code:5
        },
        {
            name:"MULTI SHIELD NIGERIA LIMITED",
            address: "322, Ikorodu Road Anthony Village Lagos",
            phone:"08053223090",
            zone:"National",
            code:6
        },
        {
            name:"UNITED HEALTHCARE INTERNATIONAL LTD",
            address: "7, Jaba Close. off Dunukofia street, Opp. FCDA Minister’s Gate Area 11, Garki, Abuja",
            phone:"08160000061",
            zone:"National",
            code:7
        },
        {
            name:"PREMIUM HEALTH LTD",
            address: "31B, Itafaji Road, Dolphin Estate,Ikoyi, Lagos",
            phone:"08051680494",
            zone:"National",
            code:8
        },
        {
            name:"Ronsberger Nigeria Limited",
            address: "No. 30 Ubiaja Street, Garki, Abuja",
            phone:"07051644470",
            zone:"National",
            code:10
        },
        {
            name:"INTERNATIONAL HEALTH MANAGEMENT SERVICES LIMITED",
            address: "2, Joseph Street, Off Broad Street, Lagos",
            phone:"08085862123",
            zone:"National",
            code:11
        },
        {
            name:"CAPEX EXPATCARE LIMITED",
            address: "Emmanuel Plaza, 23B Fatai Atere Way Matori, Lagos.",
            phone:"08055890010",
            zone:"National",
            code:12
        }, {
            name:"SONGHAI HEALTH TRUST LTD",
            address: "Ground Floor, Nigeria Re-Insurance Building Beside Unity Bank, Plot 784A Herbert Macaulay Way, Central Area, Abuja",
            phone:"08033584332",
            zone:"National",
            code:13
        }, {
            name:"INTEGRATED HEALTHCARE LTD",
            address: "12 Jos Street, Area 3, Garki, Abuja",
            phone:"08070833373",
            zone:"National",
            code:14
        },{
            name:"PREMIER MEDICAID LTD",
            address: "UBN Building Opp. Mobil Petrol Station New Ife Road Ibadan",
            phone:"08063446465",
            zone:"National",
            code:15
        },{
            name:"MANAGED HEALTHCARE SERVICES LTD",
            address: "Lagos Office: 16 Obokun Street, Off Coker Road, Oshodi Lagos.",
            phone:"08035068857",
            zone:"National",
            code:16
        },{
            name:"PRINCETON HEALTH GROUP",
            address: "25, Mogaji-Are Road Opposite D-Rovans Hotel Ring Road Mapo, Ibadan.",
            phone:"08034306380",
            zone:"National",
            code:17
        },{
            name:"MAAYOIT HEALTHCARE LTD",
            address: "1 Iloffa Road, G.R.A. Ilorin, Kwara State.",
            phone:"08058026841",
            zone:"National",
            code:18
        },{
            name:"Wise Health Services Limited",
            address: "27 Addis Ababa Crescent, Wuse Zone 4, Abuja",
            phone:"08038183442",
            zone:"National",
            code:19
        },{
            name:"WETLANDS HEALTH SERVICES LTD.",
            address: "Plot 280B Danjuma Drive Off Peter Odili Road, Trans Amadi Port Harcourt, Rivers State",
            phone:"08055026610",
            zone:"National",
            code:20
        },{
            name:"ZENITH MEDICARE LTD.",
            address: "11Lagtang Close, Area 3 Garki, Abuja",
            phone:"08087339159",
            zone:"National",
            code:21
        },{
            name:"DEFENCE HEALTH MAINTENANCE LTD.",
            address: "Plot 1323, Adesoji Aderemi Street Gudu District, Abuja.",
            phone:"08075801389",
            zone:"National",
            code:22
        },{
            name:"UNITED COMPREHENSIVE HEALTH MANAGERS LTD",
            address: "Plot C1, Rumuogba Trans Amadi Port Harcourt Rivers State",
            phone:"08033419470",
            zone:"National",
            code:23
        },
          {
            name:"HEALTHCARE SECURITY LTD",
            address: "Plot C1, Rumuogba Trans Amadi Port Harcourt Rivers State",
            phone:"08033148050",
            zone:"National",
            code:24
        }, {
            name:"ROYAL HEALTH MAINTENANCE SERVICES",
            address: "12 Cause way/24 Wetheral road, Owerri",
            phone:"08037956689",
            zone:"National",
            code:26
        },{
            name:"ZUMA HEALTH TRUST",
            address: "Block 6 Flat 7, Abak Close Off Ibadan Street (Opp. Catholic Pro-Cathedral) Area 3 Garki, Abuja.",
            phone:"07063650092",
            zone:"National",
            code:28
        },
        {
            name:"MARFEMA NIGERIA LIMITED",
            address: "4A Gurara (Lumsar) Street, Ibrahim Abacha Estate, Zone 4, Abuja.",
            phone:"08036234377",
            zone:"National",
            code:29
        },{
            name:"PREPAID MEDICARE SERVICES LTD.",
            address: "1 Bangui Street Off Adetokunbo Ademola Crescent,Beside Chicken House,Wuse II Abuja",
            phone:"07027861384",
            zone:"National",
            code:30
        },
        {
            name:"KBL HEALTHCARE LIMITED",
            address: "371, Borno Way Alagomeji Yaba Lagos",
            phone:"07069728007",
            zone:"National",
            code:34
        },{
            name:"STERLING HEALTH MANAGED CARE SERVICES LIMITED",
            address: "Valley View Plaza 99 Opebi Road Ikeja Lagos",
            phone:"08191117422",
            zone:"National",
            code:33
        },{
            name:"PRECIOUS HEALTHCARE LIMITED",
            address: "No. 8 Lungi Street Off Cairo Wuse II Abuja",
            phone:"08085556471",
            zone:"National",
            code:36
        },{
            name:"OCEANIC HEALTH MANAGEMENT LIMITED",
            address: "Oceanic Bank NAFCON House Branch, 264, Murtala Muhammed Way, Ebute Metta",
            phone:"08120220255",
            zone:"National",
            code:39
        },{
            name:"INVESTCORP MEDICARE LTD",
            address: "Bufallo House No 2 Allen Avenue Ikeja Lagos.",
            phone:"08057280794",
            zone:"National",
            code:41
        },{
            name:"GREENBAY HEALTHCARE SERVICES LTD",
            address: "Suite AF 12, Annex A White House Metro Plaza Abuja.",
            phone:"07042328818",
            zone:"National",
            code:43
        },{
            name:"MEDEXIA Ltd",
            address: "221 Ikorodu Road Lagos.",
            phone:"08057970069",
            zone:"National",
            code:44
        },{
            name:"ROYAL EXCHANGE HEALTHCARE LTD",
            address: "13 Oke Olowogbowo Street Apongbon Lagos",
            phone:"08053775743",
            zone:"National",
            code:46
        },{
            name:"MARINA MEDICAL SERV. HMO LTD",
            address: "24 Montgomery Road Yaba Lagos",
            phone:"07023028780",
            zone:"National",
            code:48
        },
        {
            name:"NONSUCH MEDICARE LTD",
            address: "Wema Bank Plaza 2nd Floor Suite 3 CBD, Abuja",
            phone:"08034080055",
            zone:"National",
            code:51
        },{
            name:"SALUS TRUST GTE LTD",
            address: "Catholic Secretariat of Nigeria Force Road Tafawa Balewa Square Lagos.",
            phone:"08062910443",
            zone:"National",
            code:54
        },{
            name:"PROHEALTH HMO LTD",
            address: "62 Lobito Crescent Off Ademola Adetokunbo Wuse II Abuja",
            phone:"08062691967",
            zone:"National",
            code:57
        },
        {
            name:"MANSARD HEALTH PLAN NIGERIA LTD",
            address: "18A Dapo Solanke Close Lekki Phase Lagos.",
            phone:"08025144905",
            zone:"National",
            code:59
        },{
            name:"GNI HEALTHCARE NIGERIA LTD",
            address: "Plot 32 Western Avenue Alaka Surulere Lagos",
            phone:"08033524202",
            zone:"Zonal- SW",
            code:60
        },{
            name:"ULTIMATE HEALTH MANAGEMENT SERVICES LTD",
            address: "Wema Bank Building 4th Floor Airport Return Road Central Business District Abuja",
            phone:"08060665572",
            zone:"National",
            code:61
        },
        {
            name:"AVON HEALTHCARE LIMITED",
            address: "29 Rumens Road Ikoyi, Lagos",
            phone:"07002779800",
            zone:"National",
            code:63
        }, {
            name:"REGENIX HEALTH SERVICES LIMITED",
            address: "Kieni Plaza KLM 13 Melford Okilo Road Akena- Epie, Yenagoa",
            phone:"080374762540",
            zone:"National",
            code:64
        }, {
            name:"REDCARE HEALTH SERVICE LIMITED",
            address: "22 Coker Road Opposite Mamacass, ilupeju Lagos.",
            phone:"07098767034",
            zone:"National",
            code:65
        },
        {
            name:"HARRY HEALTH INTEGRATED SERVICES LIMITED",
            address: "10 Dutse Street, War College Gwarinpa Abuja",
            phone:"08039113311",
            zone:"National",
            code:66
        },{
            name:"WELL HEALTH NETWORK LIMITED",
            address: "15 Ibusa Avenue, Independent Layout, Enugu",
            phone:"08086250004",
            zone:"State-Enugu",
            code:67
        },{
            name:"BUPAR HEALTHCARE LIMITED",
            address: "Plot 102, Isheri Road, KOTCO House, Ojodu Berger, Ikeja",
            phone:"08023142458",
            zone:"State-Lagos",
            code:68
        },{
            name:"IVES MEDICARE",
            address: "4 Mojidi Street, Off Toyin Street Ikeja, Lagos",
            phone:"08056374823",
            zone:"State-Lagos",
            code:69
        },{
            name:"LIFECARE PARTNERS LIMITED",
            address: "123A Bisi Obadina Street,Omole Phase1, Ikeja. Lagos",
            phone:"08134975325",
            zone:"Nigeria",
            code:70
        },{
            name:"DOMA HEALTHCARE LTD",
            address: "Suite 9-12, 2nd Floor Adamu Fura House Adjacent ECOBANK Biu Road Gombe",
            phone:"08134975325",
            zone:"State-Gombe",
            code:71
        },
        {
            name:"Allied HMO Ltd",
            address: "1, Alhaji Animashaun Street Ajeigbe Bus Stop Ring Road Ibadan",
            phone:"08081761429",
            zone:"Zonal-SW",
            code:72
        },
        {
            name:"POLICE HEALTH MAINTENANCE LIMITED",
            address: "21, Kainji Crescent Off Lake Chad Street Maitama, Abuja.",
            phone:"07026305673",
            zone:"National",
            code:73
        },{
            name:"NOVO HEALTH AFRICA LTD",
            address: "1, Adekola Balogun Street Off Remi Olowude Street Lekki Phase I, Lagos",
            phone:"08174602236",
            zone:"National",
            code:74
        },{
            name:"MH HEALTHCARE LTD",
            address: "6A Alegbe Close Off Sunmola Street Mende Maryland Lagos.",
            phone:"08060426239",
            zone:"National",
            code:75
        },{
            name:"ANCHOR HMO INTERNATIONAL COMPANY LTD",
            address: "Goldcrest Mall Building Km 23 Lekki-Epe Express way, Lagos.",
            phone:"07080601192",
            zone:"National",
            code:76
        },
        {
            name:"METROHEALTH HMO LIMITED",
            address: "14th Floor, St. Nicholas House, 26 Catholic Mission Street, Lagos Island",
            phone:"01-4606790",
            zone:"National",
            code:77
        },
        {
            name:"GREENFIELD HEALTH MANAGEMENT LTD",
            address: "5B ihiala Street,Independence Layout, Enugu State. ",
            phone:"08135836235",
            zone:"National",
            code:78
        },{
            name:"LIFE WORTH MEDICARE LTD.",
            address: "NA ",
            phone:"NA",
            zone:"State-Lagos",
            code:79
        }
   ]

    Hmo.create(HMOS).then(function(data){
       console.log("success");
    },function(err){
        if(err.statusCode!=422){
           // console.log(err)
        }
    })




















}