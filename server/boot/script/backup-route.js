var backup = require('mongodb-backup');
var multer  = require('multer');
var upload = multer({ dest: __dirname })
var restore = require('mongodb-restore'); // use require('mongodb-restore') instead
var fs = require('fs');
module.exports = function(app){



    app.get('/backupData/storeBackup', function(req, res) {
        var theDate = new Date();
        backup({
            uri: 'mongodb://localhost:27017/medirec-api-loopback-development',
            root: __dirname, // write files into this dir
            tar: theDate+'-backup.tar'
        });
       var objBackup={};
        objBackup.createdAt = new Date();
        objBackup.filename = theDate+'-backup.tar';
       app.models.Backup.create(objBackup)
           .then(function(data){
               res.status(200).json({data:"success"});
           },function(error){
               res.status(500).json({error:error});
           })
    }) ;
    app.get('/backupData/downloadData', function(req, res) {
        res.writeHead(200, {
            'Content-Type': 'application/x-tar' // force header for tar download
        });
        backup({
            uri: 'mongodb://localhost:27017/medirec-api-loopback-development',
            root: __dirname, // write files into this dir
            tar: new Date()+'-backup.tar',
            stream: res // save backup inot this tar file

        });
    });

    app.get('/backupData/getLatestBackup',function(req,res){
       app.models.Backup.find({}).
           then(function(data){
               res.status(200).json(data)
           },function(data){
               res.status(500).json(data)
           })
    });
    app.get('/backupData/downloadBackup/:id',function(req,res){
        var id = req.params.id
        app.models.Backup.findById(id)
            .then(function(data){
                res.download(__dirname+'/'+data.filename,'backup.tar');
            },function(err){
                res.status(500).json(err)
            })
    })
    app.post('/backupData/uploadRestoreFile',upload.single('file'),function(req,res){
        var file = req.file;
        console.log(req);
        var tmp_path = req.file.path;
        var dest_final = new Date()+req.file.originalname;
        var target_path = __dirname +'/'+dest_final;
        var src = fs.createReadStream(tmp_path);
        var dest = fs.createWriteStream(target_path);
        src.pipe(dest);
        src.on('end', function() {  var objBackup={};
            objBackup.createdAt = new Date();
            objBackup.filename = dest_final;
            app.models.Backup.create(objBackup)
                .then(function(data){
                    console.log(data)
                    res.status(200).json({data:"success"});
                },function(error){
                    res.status(500).json({error:error});
                })
        })
        src.on('error', function(err) { res.status(500).json({errorMessage:err}) });
    })

    app.get('/backupData/restoreData/:id',function(req,res){
        app.models.Backup.findById(req.params.id)
            .then(function(data){
                restore({
                    uri: 'mongodb://localhost:27017/medirec-api-loopback-development',
                    root: __dirname, // read tar file from this dir
                    tar: data.filename // restore backup from this tar file
                });

            },function(error){
                res.status(500).json(error);

            })
    })


}